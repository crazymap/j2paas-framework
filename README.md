#  **[ J2PaaS——企业级开发工具 ](http://jikaiyun.com)** 

### 一、介绍
J2PaaS是吉鼎科技基于20年技术沉淀和项目经验而研发，以“把编程变简单，甚至不需要编程”为核心理念，打破传统开发用“代码编程”驱动产品的模式，升级为用“参数”驱动产品，集开发引擎、运行引擎、项目管理等于一体，覆盖软件项目需求分析、设计、开发、测试、运行、维护与管理等全过程，是可视化、组件化、拖拽式开发的在线低代码敏捷开发平台。

J2PaaS平台把常规需要程序员完成的功能集成在框架中，开发人员只需关注业务逻辑，页面组装及流程设计等相关功能已经内置完成，它不仅是一个开发平台，还是强有力的生产力工具，能够实现低成本、高效率的软件系统定制开发及应用。

 **必看资源** 

- [J2PaaS--Studio开发工具下载](https://gitee.com/j2paas/j2paas-studio)（开发引擎，必须下载）
- [J2PaaS--Examples下载](http://gitee.com/j2paas/j2paas-examples)（示例项目，开发案例）
- [J2PaaS--Templates下载](http://gitee.com/j2paas/j2paas-templates)（前端页面模板开发框架）

 **系统演示** 

[开发工具演示](https://jikaiyun.com/Points)

[开发案例演示](https://jikaiyun.com/Points)

### 二、使用J2PaaS开发可提供以下优势：

- 低代码开发

  低代码可视化开发，所见即所得，能够实现软件的快速开发

- 在线协同开发

  Web在线开发，不受地理位置的限制，同时能实现在线协同开发，提高开发效率

- 集成化开发

  集成物联网、大数据等各种互通接口，可实现对不同数据的对接，无需再开发，轻松对接

- 即时生效

  业务需求变更，无需重新部署，使用参数（元数据）驱动，随时灵活修改且即刻生效

- 在线调试

  提供引用的栏位、变量、方法调用、异常轨迹以及自定义输出等等信息的呈现

- api接口

  统一的接口访问方式，通过参数配置，在线测试，实现数据多样性供给第3方应用调用

- 应用监控管理

  每一套应用都有独立的管理系统，包括用户、日志、连接池、缓存等资源的管理和监控

- 插件式服务

  通过扩展spi接口，平台可以实现Iot、RPC、工作流引擎等第3方服务的应用

- 国产化支持

  通过信创体系认证和华为云鲲鹏云兼容性认证，可适配各种国产化软硬件，包括CPU芯片、处理器、操作系统、服务器、数据库、中间件、浏览器等

### 三、开发环境搭建

- 本教程假设您已安装好mysql、jdk、git以及相关的开发工具（idea或eclipse).

1. 执行以下命令：

~~~shell
   > git clone https://gitee.com/j2paas/j2paas-framework.git
   > cd j2paas-framework
   > mvn install:install-file "-Dfile=j2paas-web/lib/itext.jar" "-DgroupId=com.lowagie"  "-DartifactId=itext" "-Dversion=2.1.7.js6" "-Dpackaging=jar"
   > mvn install
~~~

***上面install-file是为了解决jasperreports依赖不存在的itext库导致打包失败，请务必先执行！***

2. 您需要[下载示例案例](https://gitee.com/j2paas/j2paas-examples)

~~~shell
  > git clone https://gitee.com/j2paas/j2paas-examples.git
~~~

3. 选择gnys案例，拷贝gnys目录下的scheme.zip、template.zip、 workspace.zip到j2paas-web/WebRoot/WEB-INF下面，分别解压

~~~shell
   > cd j2paas-web
   > mvn jetty:run-war
~~~

4. 打开浏览器，输入http://localhost:8080/j2paas，打开网页，会提示：

![install0.jpg](j2paas-docs/img/install/install0.png)

5. 点击初始化环境

![install1.jpg](j2paas-docs/img/install/install1.png)

6. 点击“下一步”，开始导入数据，导入的数据比较多，要耐心等待一段时间

![install2.jpg](j2paas-docs/img/install/install2.png)

7. 导入成功后，点击“下一步”，启动服务

![install3.jpg](j2paas-docs/img/install/install3.png)

8. 服务启动成功

![install4.jpg](j2paas-docs/img/install/install4.png)

9. 点击”跳转到登录页面“

<br/>

![install5.jpg](j2paas-docs/img/install/install5.png)

10. 输入项目案例提供的用户和密码登录系统。

- 手动导入案例

1. 拷贝scheme.zip、template.zip、 workspace.zip到部署目录WEB-INF下面，分别解压
2. 创建参数数据库，导入app_metadata.sql
3. 创建业务数据库，导入app_biz.sql
4. 新建mysql用户或已存在的用户，并给参数数据库和业务数据库授权
5. 修改WEB-INF/conf/easyplatform.conf文件，配置参数数据库连接
     <br/>
   (数据库连接密码 password 的值需要用 cn.easyplatform.util.ConfigTools.encrypt 进行加密)
6. 配置开发工具(idea或eclipse)Web服务器，部署j2paas-web


### 四、开源版与企业版区分
| 名称 | 开源版 |企业版 | 说明 |
|--- | --------|------|-----|
|表单(桌面/移动)|	√|	√|
|逻辑|	√|	√|
|JasperReport报表|	√|	√|
|定时任务	|√|	√|
|批处理	|√	|√|
|工作流引擎	|√	|√	|企业版支持flowable、activity|
|工作流在线设计	|√	|√|
|Excel报表	|√	|√|
|权限管理系统	|√	|√|
|自定义函数	|√	|√|
|自定义（插件）服务	|√	|√|
|API开发	|√	|√|
|第三方登录	|√	|√	|微信、QQ、钉钉、OPENAPI、OAUTH|
|单点登陆	|√	|√|
|匿名访问	|√	|√|
|黑白名单	|√	|√|
|在线监控	|√	|√|
|多国语言支持	|√	|√|
|登录页模板化	|√	|√|
|主页模板化	|√	|√|
|消息系统	|√	|√	|通知、公告、新闻|
|功能引导	|√	|√|
|自定义控件	|√	|√|
|数据字典	|√	|√|
|图表集成	|√	|√	|企业版还支持highcharts|
|第三方功能集成	|√	|√	|邮件、短信、微信….|
|国产化支持	|√	|√	|数据库、中间件、操作系统|
|Redis缓存/Session	|√	|√|
|页面风格	|√	|√|
|单项目多门户	|√	|√|
|对象存储	|	|√	|阿里云oss、华为云obs、腾讯云cos、七牛云及自定义分布式文件系统|
|BI大屏		| |√|
|IoT物联网模块 | |√|
|分库分表		| |√|
|集成门户系统	| |	√|
|金融SWIFT模块| |√|
|Word报表模块	| |	√|
|规则引擎	| |	√|
|逻辑引擎| |√| 开源版支持js，企业版增加java支持 |
|PaaS支持| |	√|
|RPC支持 | |	√|
|分布式集群| |	√|

### 五、相关资源

- [下载J2PaaS Studio开发工具](https://gitee.com/j2paas/j2paas-studio "下载J2PaaS Studio开发工具")
- [学习教程](https://ke.qq.com/course/3737600?taid=12500476548941824)
- [立即体验](https://jikaiyun.com/Points)
- [文档中心](http://zj-docs.51epedu.com/#/)
- [开源社区](https://bbs.jikaiyun.com/)
- [软说商城](https://softshuo.com/)
- QQ 群：189634425

### 六、合作伙伴

![partner.jpg](j2paas-docs/img/partner//case-main.jpg)

![partner.jpg](j2paas-docs/img/partner//case-erp.jpg)`这里输入代码`