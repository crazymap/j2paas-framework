/*
 * create by 陈云亮(shiny_vc@163.com)
 */
(function(mod) {
  if (typeof exports == "object" && typeof module == "object")
    mod(require("../../lib/codemirror"));
  else if (typeof define == "function" && define.amd)
    define(["../../lib/codemirror"], mod);
  else
    mod(CodeMirror);
})(function(CodeMirror) {
	"use strict";
	CodeMirror.defineSimpleMode("epscript", {
	  start: [
	    {regex: /"(?:[^\\]|\\.)*?"/, token: "string"},
	    {regex: /'(?:[^\\]|\\.)*?'/, token: "string"},
	    {regex: /(function)(\s+)([a-z$][\w$]*)/,
	     token: ["keyword", null, "variable-2"]},
	    {regex: /(?:function|var|return|if|for|while|else|do|this|in|import|try|catch|throw|finally|break|continue|new|delete|switch|case|default|typeof|instanceof|extends|class|super|parent)\b/,
	     token: "keyword"},
	    {regex: /true|false|null|undefined|NaN|Infinity/, token: "atom"},
	    {regex: /0x[a-f\d]+|[-+]?(?:\.\d+|\d+\.?\d*)(?:e[-+]?\d+)?/i,
	     token: "number"},
	    {regex: /\/\/.*/, token: "comment"},
	    {regex: /\/\*/, token: "comment", next: "comment"},
	    {regex: /[-+\/*=<>!]+/, token: "operator"},
	    {regex: /[\{\[\(]/, indent: true},
	    {regex: /[\}\]\)]/, dedent: true},
	    {regex: /(?:\$|@|&|#include|#app|#db|#dt|#util|#bpm)\b/, token: "variable-3"},
	    {regex: /(?:cache|exists|createVariable|getValue|setValue|load|getSerial|resetSerial|deleteSerial|invoke|go|debug|isEmpty|isEquals|isDefine|print|getObject|i18n)\b/, token: "variable-4"}
	  ],
	  comment: [
	    {regex: /.*?\*\//, token: "comment", next: "start"},
	    {regex: /.*/, token: "comment"}
	  ],
	  meta: {
	    dontIndentStates: ["comment"],
	    lineComment: "//",
		electricInput: /^\s*(?:case .*?:|default:|\{|\})$/,
		blockCommentStart: "/*",
		blockCommentEnd: "*/",
		fold: "brace",
		closeBrackets: "()[]{}''\"\"``",
		helperType: "epscript"
	  },
	});
	CodeMirror.defineMIME("text/epscript", "epscript");
});
