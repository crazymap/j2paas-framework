/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.carousel;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.web.annotation.Builder;
import cn.easyplatform.web.ext.Reloadable;
import cn.easyplatform.web.ext.Widget;
import cn.easyplatform.web.ext.ZkExt;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import org.apache.commons.lang3.math.NumberUtils;
import org.zkoss.zk.au.AuRequest;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.impl.XulElement;

@Builder(CarouselBuilder.class)
public class Carousel extends XulElement implements ZkExt,Reloadable {

    public final static String ON_Link = "onLink";

    static {
        addClientEvent(Carousel.class, ON_Link, CE_IMPORTANT );
    }

    private String[] img;
    private String[] title;
    private String[] content;
    private String[] imgurl;
    private String[] imgevt;
    private String[] sequence;
    private int intervalTime;

    private String query;
    private String dbId;
    private String filter;
    private boolean reloadFlag;

    public String[] getImg() {
        return img;
    }

    public void setImg(String img) {
        if(!Strings.isBlank(img)){
            try {
                JSONArray js = JSON.parseArray(img);
                this.img = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("img", this.img);
        }

    }

    public String[] getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if(!Strings.isBlank(title)){
            try {
                JSONArray js = JSON.parseArray(title);
                this.title = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("title", this.title);
        }
    }

    public String[] getContent() {
        return content;
    }

    public void setContent(String content) {
        if(!Strings.isBlank(content)){
            try {
                JSONArray js = JSON.parseArray(content);
                this.content = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("content", this.content);
        }
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        if(!Strings.isBlank(query)) {
            this.query = query;
            smartUpdate("query", this.query);
        }
    }

    public String getDbid() {
        return dbId;
    }

    public void setDbid(String dbId) {
        if(!Strings.isBlank(dbId)) {
            this.dbId = dbId;
            smartUpdate("dbId", this.dbId);
        }
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        if(!Strings.isBlank(filter)) {
            this.filter = filter;
            smartUpdate("filter", this.filter);
        }
    }

    public String[] getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        if(!Strings.isBlank(sequence)) {
            try {
                JSONArray js = JSON.parseArray(sequence);
                this.sequence = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("sequence", this.sequence);
        }
    }

    public String[] getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        if(!Strings.isBlank(imgurl)){
            try {
                JSONArray js = JSON.parseArray(imgurl);
                this.imgurl = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("imgurl", this.imgurl);
        }
    }

    public String[] getImgevt() {
        return imgevt;
    }

    public void setImgevt(String imgevt) {
        if(!Strings.isBlank(imgevt)){
            try {
                JSONArray js = JSON.parseArray(imgevt);
                this.imgevt = js.toArray(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
            smartUpdate("imgevt", this.imgevt);
        }
    }

    public boolean isReloadFlag() {
        return reloadFlag;
    }

    public void setReloadFlag(boolean reloadFlag) {
        this.reloadFlag = reloadFlag;
        smartUpdate("reloadFlag", this.reloadFlag);
    }

    public int getIntervalTime() {
        return intervalTime;
    }

    public void setIntervalTime(String intervalTime) {
        if(!Strings.isBlank(intervalTime)&& NumberUtils.isNumber(intervalTime)){
            int time = Integer.parseInt(intervalTime);
            if(time>0){
                this.intervalTime = time;
            }else{
                this.intervalTime=1;
            }
        }
    }

    //super//
    protected void renderProperties(org.zkoss.zk.ui.sys.ContentRenderer renderer)
            throws java.io.IOException {
        super.renderProperties(renderer);

        render(renderer, "img", img);
        render(renderer, "title", title);
        render(renderer, "content", content);
        render(renderer, "imgurl", imgurl);
        render(renderer, "imgevt", imgevt);
        render(renderer, "intervalTime", intervalTime);
        render(renderer, "query", query);
        render(renderer, "dbId", dbId);
        render(renderer, "filter", filter);
    }

    public void service(AuRequest request, boolean everError) {
        final String cmd = request.getCommand();

        if (cmd.equals(ON_Link)) {
            Event evt = new Event(cmd, this, request.getData().get("data"));
            Events.postEvent(evt);
        }
    }

    /**
     * The default zclass is "z-carousel"
     */
    public String getZclass() {
        return (this._zclass != null ? this._zclass : "z-carousel");
    }


    public void reload() {
        Widget ext = (Widget) getAttribute("$proxy");
        ext.reload(this);
    }

    public boolean isForce() {
        return false;
    }

}

