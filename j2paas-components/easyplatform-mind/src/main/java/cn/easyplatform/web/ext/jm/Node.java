/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.jm;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.*;

/**
 * @Author: <a href="mailto:davidchen@epclouds.com">陈云亮</a> <br/>
 * @Description:
 * @Since: 2.0.0 <br/>
 * @Date: Created in 2019/11/14 13:12
 * @Modified By:
 */
public class Node {

    private Object id;        //  : string                    节点id
    private String topic;    //  : string                    节点主题
    private boolean isroot;    //  : boolean                   指示该节点是否为根节点
    private Node parent;    //  : node                      该节点的父节点，根节点的父节目为 null ，但请不要根据此属性判断该节点是否为根节点
    private String direction; //  : enum(left,center,right)   该节点的分布位置
    private List<Node> children;  //  : array of node             该节点的子节点组合
    private boolean expanded;  //  : boolean                   该节点的下级节点是否展开
    private Map<String, Object> data;      //  : object{string,object}     该节点的其它附加数据

    @JsonIgnore
    private Object value;

    public Node(Object id) {
        this.id = id;
    }

    public Node(Object id, String topic) {
        this.id = id;
        this.topic = topic;
    }


    public Object getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public boolean isIsroot() {
        return isroot;
    }

    public void setIsroot(boolean isroot) {
        this.isroot = isroot;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public List<Node> getChildren() {
        return children;
    }

    public void setChildren(List<Node> children) {
        this.children = children;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public void setBackgroundColor(String color) {
        if (data == null)
            data = new HashMap<>();
        data.put("background-color", color);
    }

    public void setForegroundColor(String color) {
        if (data == null)
            data = new HashMap<>();
        data.put("foreground-color", color);
    }

    public void setWidth(int width) {
        if (data == null)
            data = new HashMap<>();
        data.put("width", width);
    }

    public void setHeight(int height) {
        if (data == null)
            data = new HashMap<>();
        data.put("height", height);
    }

    public void setFontSize(int fontSize) {
        if (data == null)
            data = new HashMap<>();
        data.put("font-size", fontSize);
    }

    public void setFontWeight(int fontWeight) {
        if (data == null)
            data = new HashMap<>();
        data.put("font-weight", fontWeight);
    }

    public void setFontStyle(String fontStyle) {
        if (data == null)
            data = new HashMap<>();
        data.put("font-style", fontStyle);
    }

    public void setBackgroundImage(String backgroundImage) {
        if (data == null)
            data = new HashMap<>();
        data.put("background-image", backgroundImage);
    }

    public void setBackgroundRotation(int backgroundRotation) {
        if (data == null)
            data = new HashMap<>();
        data.put("background-rotation", backgroundRotation);
    }

    public void appendChild(Node child) {
        if (children == null)
            children = new ArrayList<>();
        child.setParent(new Node(this.id));
        children.add(child);
    }

    public Node appendChild(Object id, String topic) {
        return appendChild(id, topic, null, true);

    }

    public Node appendChild(Object id, String topic, String direction) {
        return appendChild(id, topic, direction, true);
    }

    public Node appendChild(Object id, String topic, String direction, boolean expanded) {
        Node child = new Node(id, topic);
        child.setParent(new Node(this.id));//如果直接引用this,会导致json出错
        child.setDirection(direction);
        child.setExpanded(expanded);
        if (children == null)
            children = new ArrayList<>();
        children.add(child);
        return child;
    }

    public void removeChild(Node child) {
        if (children != null)
            children.remove(child);
    }

    public void removeChild(Object id) {
        if (children != null) {
            Node node = null;
            for (Node child : children) {
                if (Objects.equals(id, child.id)) {
                    node = child;
                    break;
                }
            }
            if (node != null)
                children.remove(node);
        }
    }

    public void clear() {
        if (children != null)
            children.clear();
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Node))
            return false;
        return id.equals(((Node) obj).id);
    }

    @Override
    public String toString() {
        return new StringBuilder().append(id).append("(").append(topic).append(")").toString();
    }
}
