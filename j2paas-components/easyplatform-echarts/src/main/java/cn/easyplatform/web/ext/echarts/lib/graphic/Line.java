/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.graphic;

import cn.easyplatform.web.ext.echarts.lib.graphic.style.GraphicStyle;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Line extends Graphic {

    private Shape shape;

    private GraphicStyle style;

    public Line() {
        this.type("line");
    }

    public Shape shape() {
        if (shape == null)
            shape = new Shape();
        return shape;
    }

    public Line shape(Shape shape) {
        this.shape = shape;
        return this;
    }

    public GraphicStyle style() {
        if (style == null)
            style = new GraphicStyle();
        return style;
    }

    public Line style(GraphicStyle style) {
        this.style = style;
        return this;
    }

    public static class Shape {
        /**
         * 图形元素的左上角在父节点坐标系（以父节点左上角为原点）中的横坐标值
         */
        private Integer x;
        /**
         * 图形元素的左上角在父节点坐标系（以父节点左上角为原点）中的横坐标值
         */
        private Integer x1;
        /**
         * 图形元素的左上角在父节点坐标系（以父节点左上角为原点）中的纵坐标值
         */
        private Integer y;
        /**
         * 图形元素的左上角在父节点坐标系（以父节点左上角为原点）中的纵坐标值
         */
        private Integer y1;
        /**
         * 线画到百分之多少就不画了。值的范围：[0, 1]
         */
        private Double percent;

        public Integer x() {
            return this.x;
        }

        public Shape x(Integer x) {
            this.x = x;
            return this;
        }

        public Integer y() {
            return this.y;
        }

        public Shape y(Integer y) {
            this.y = y;
            return this;
        }
        public Integer x1() {
            return this.x1;
        }

        public Shape x1(Integer x1) {
            this.x1 = x1;
            return this;
        }

        public Integer y1() {
            return this.y1;
        }

        public Shape y1(Integer y1) {
            this.y1 = y1;
            return this;
        }
        public Double percent() {
            return this.percent;
        }

        public Shape percent(Double percent) {
            this.percent = percent;
            return this;
        }

        public Integer getX() {
            return x;
        }

        public void setX(Integer x) {
            this.x = x;
        }

        public Integer getX1() {
            return x1;
        }

        public void setX1(Integer x1) {
            this.x1 = x1;
        }

        public Integer getY() {
            return y;
        }

        public void setY(Integer y) {
            this.y = y;
        }

        public Integer getY1() {
            return y1;
        }

        public void setY1(Integer y1) {
            this.y1 = y1;
        }

        public Double getPercent() {
            return percent;
        }

        public void setPercent(Double percent) {
            this.percent = percent;
        }
    }
}
