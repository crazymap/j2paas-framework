/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.threeD;

import cn.easyplatform.web.ext.echarts.lib.axis.BaseAxis;

import java.io.Serializable;

public class Axis3D extends BaseAxis implements Serializable {
    /**
     * 是否显示轴。
     */
    private boolean show;
    /**
     * 坐标轴使用的 grid3D 组件的索引。默认使用第一个 grid3D 组件。
     */
    private int grid3DIndex;

    public Boolean show() {
        return this.show;
    }
    public Axis3D show(Boolean show) {
        this.show = show;
        return this;
    }

    public Integer grid3DIndex() {
        return this.grid3DIndex;
    }
    public Axis3D grid3DIndex(Integer grid3DIndex) {
        this.grid3DIndex = grid3DIndex;
        return this;
    }

    public boolean isShow() {
        return show;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public int getGrid3DIndex() {
        return grid3DIndex;
    }

    public void setGrid3DIndex(int grid3DIndex) {
        this.grid3DIndex = grid3DIndex;
    }
}
