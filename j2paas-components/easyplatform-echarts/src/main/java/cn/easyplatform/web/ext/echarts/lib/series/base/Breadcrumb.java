/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series.base;

import cn.easyplatform.web.ext.echarts.lib.style.Emphasis;
import cn.easyplatform.web.ext.echarts.lib.style.ItemStyle;

import java.io.Serializable;

/**
 * 多层配置 - treemap
 *
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Breadcrumb implements Serializable {
    private Boolean show;
    private Object left;
    private Object top;
    private Object right;
    private Object bottom;
    private Object height;
    private Object emptyItemWidth;
    private ItemStyle itemStyle;
    private Emphasis emphasis;

    public Emphasis emphasis() {
        if (emphasis == null)
            emphasis = new Emphasis();
        return emphasis;
    }

    public Breadcrumb emphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
        return this;
    }

    public Boolean show() {
        return show;
    }

    public Breadcrumb show(Boolean show) {
        this.show = show;
        return this;
    }

    public Object left() {
        return left;
    }

    public Breadcrumb left(Object left) {
        this.left = left;
        return this;
    }

    public Object top() {
        return top;
    }

    public Breadcrumb top(Object top) {
        this.top = top;
        return this;
    }

    public Object right() {
        return right;
    }

    public Breadcrumb right(Object right) {
        this.right = right;
        return this;
    }

    public Object bottom() {
        return bottom;
    }

    public Breadcrumb bottom(Object bottom) {
        this.bottom = bottom;
        return this;
    }

    public Object height() {
        return height;
    }

    public Breadcrumb height(Object height) {
        this.height = height;
        return this;
    }

    public Object emptyItemWidth() {
        return emptyItemWidth;
    }

    public Breadcrumb emptyItemWidth(Object emptyItemWidth) {
        this.emptyItemWidth = emptyItemWidth;
        return this;
    }

    public ItemStyle itemStyle() {
        if (itemStyle == null)
            itemStyle = new ItemStyle();
        return itemStyle;
    }

    public Breadcrumb itemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
        return this;
    }

    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public Object getLeft() {
        return left;
    }

    public void setLeft(Object left) {
        this.left = left;
    }

    public Object getTop() {
        return top;
    }

    public void setTop(Object top) {
        this.top = top;
    }

    public Object getRight() {
        return right;
    }

    public void setRight(Object right) {
        this.right = right;
    }

    public Object getBottom() {
        return bottom;
    }

    public void setBottom(Object bottom) {
        this.bottom = bottom;
    }

    public Object getHeight() {
        return height;
    }

    public void setHeight(Object height) {
        this.height = height;
    }

    public Object getEmptyItemWidth() {
        return emptyItemWidth;
    }

    public void setEmptyItemWidth(Object emptyItemWidth) {
        this.emptyItemWidth = emptyItemWidth;
    }

    public ItemStyle getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
    }
}
