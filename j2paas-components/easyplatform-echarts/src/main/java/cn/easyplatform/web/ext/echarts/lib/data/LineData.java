/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.data;

import cn.easyplatform.web.ext.echarts.lib.Tooltip;
import cn.easyplatform.web.ext.echarts.lib.style.Emphasis;
import cn.easyplatform.web.ext.echarts.lib.style.ItemStyle;
import cn.easyplatform.web.ext.echarts.lib.style.LabelStyle;

import java.io.Serializable;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class LineData implements Serializable {

    private static final long serialVersionUID = 7218201600361155091L;

    /**
     * 名称
     */
    private String name;
    private Object value;
    private String symbol;
    private Object symbolSize;
    private Double symbolRotate;
    private Object symbolOffset;
    private Boolean symbolKeepAspect;
    private LabelStyle label;
    private ItemStyle itemStyle;
    private Emphasis emphasis;
    private Tooltip tooltip;

    public LineData(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public Tooltip tooltip() {
        if (tooltip == null)
            tooltip = new Tooltip();
        return this.tooltip;
    }

    public LineData tooltip(Tooltip tooltip) {
        this.tooltip = tooltip;
        return this;
    }

    public Emphasis emphasis() {
        if (emphasis == null)
            emphasis = new Emphasis();
        return this.emphasis;
    }

    public LineData emphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
        return this;
    }

    public Boolean symbolKeepAspect() {
        return this.symbolKeepAspect;
    }

    public LineData symbolKeepAspect(Boolean symbolKeepAspect) {
        this.symbolKeepAspect = symbolKeepAspect;
        return this;
    }

    public String name() {
        return this.name;
    }

    public LineData name(String name) {
        this.name = name;
        return this;
    }

    public Object value() {
        return this.value;
    }

    public LineData value(Object value) {
        this.value = value;
        return this;
    }

    public String symbol() {
        return this.symbol;
    }

    public LineData symbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    public Object symbolSize() {
        return this.symbolSize;
    }

    public LineData symbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
        return this;
    }

    public Double symbolRotate() {
        return this.symbolRotate;
    }

    public LineData symbolRotate(Double symbolRotate) {
        this.symbolRotate = symbolRotate;
        return this;
    }

    public Object symbolOffset() {
        return this.symbolOffset;
    }

    public LineData symbolOffset(Object symbolOffset) {
        this.symbolOffset = symbolOffset;
        return this;
    }

    public LabelStyle label() {
        if (label == null)
            label = new LabelStyle();
        return this.label;
    }

    public LineData label(LabelStyle label) {
        this.label = label;
        return this;
    }

    public ItemStyle itemStyle() {
        if (itemStyle == null)
            itemStyle = new ItemStyle();
        return this.itemStyle;
    }

    public LineData itemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Object getSymbolSize() {
        return symbolSize;
    }

    public void setSymbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
    }

    public Double getSymbolRotate() {
        return symbolRotate;
    }

    public void setSymbolRotate(Double symbolRotate) {
        this.symbolRotate = symbolRotate;
    }

    public Object getSymbolOffset() {
        return symbolOffset;
    }

    public void setSymbolOffset(Object symbolOffset) {
        this.symbolOffset = symbolOffset;
    }

    public LabelStyle getLabel() {
        return label;
    }

    public void setLabel(LabelStyle label) {
        this.label = label;
    }

    public ItemStyle getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
    }

    public Emphasis getEmphasis() {
        return emphasis;
    }

    public void setEmphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
    }

    public Boolean getSymbolKeepAspect() {
        return symbolKeepAspect;
    }

    public void setSymbolKeepAspect(Boolean symbolKeepAspect) {
        this.symbolKeepAspect = symbolKeepAspect;
    }

    public Tooltip getTooltip() {
        return tooltip;
    }

    public void setTooltip(Tooltip tooltip) {
        this.tooltip = tooltip;
    }
}
