/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.feature;

import org.zkoss.util.resource.Labels;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DataView extends Feature {
    /**
     * 是否不可编辑（只读）
     */
    private Boolean readOnly;
    /**
     * 自定义 dataView 展现函数，用以取代默认的 textarea 使用更丰富的数据编辑。可以返回 dom 对象或者 html 字符串
     */
    private Object optionToContent;
    /**
     * 在使用 optionToContent 的情况下，如果支持数据编辑后的刷新，需要自行通过该函数实现组装 option 的逻辑
     */
    private Object contentToOption;
    /**
     * 数据视图上有三个话术，默认是['数据视图', '关闭', '刷新']
     */
    private Object[] lang;
    /**
     * 数据视图浮层背景色
     */
    private String backgroundColor;
    /**
     * 数据视图浮层文本输入区背景色
     */
    private String textareaColor;
    /**
     * 数据视图浮层文本输入区边框颜色
     */
    private String textareaBorderColor;
    /**
     * 文本颜色
     */
    private String textColor;
    /**
     * 按钮颜色
     */
    private String buttonColor;
    /**
     * 按钮文本颜色
     */
    private String buttonTextColor;

    private String icon;

    /**
     * 构造函数
     */
    public DataView() {
        this.show(true);
        this.title(Labels.getLabel("echarts.data.view"));
        this.readOnly(false);
        String lang = Labels.getLabel("echarts.data.view.lang");
        if (lang != null)
            this.lang(lang.split(","));
    }

    public String icon() {
        return this.icon;
    }

    public Feature icon(String icon) {
        this.icon = icon;
        return this;
    }

    public Boolean readOnly() {
        return this.readOnly;
    }

    public Feature readOnly(Boolean readOnly) {
        this.readOnly = readOnly;
        return this;
    }

    public Object optionToContent() {
        return this.optionToContent;
    }

    public DataView optionToContent(Boolean optionToContent) {
        this.optionToContent = optionToContent;
        return this;
    }

    public Object contentToOption() {
        return this.contentToOption;
    }

    public DataView contentToOption(Boolean contentToOption) {
        this.contentToOption = contentToOption;
        return this;
    }

    public Object[] lang() {
        return this.lang;
    }

    public DataView lang(Object[] lang) {
        this.lang = lang;
        return this;
    }

    public String backgroundColor() {
        return this.backgroundColor;
    }

    public DataView backgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
        return this;
    }

    public String textareaColor() {
        return this.textareaColor;
    }

    public DataView textareaColor(String textareaColor) {
        this.textareaColor = textareaColor;
        return this;
    }

    public String textareaBorderColor() {
        return this.textareaBorderColor;
    }

    public DataView textareaBorderColor(String textareaBorderColor) {
        this.textareaBorderColor = textareaBorderColor;
        return this;
    }

    public String textColor() {
        return this.textColor;
    }

    public DataView textColor(String textColor) {
        this.textColor = textColor;
        return this;
    }

    public String buttonColor() {
        return this.buttonColor;
    }

    public DataView buttonColor(String buttonColor) {
        this.buttonColor = buttonColor;
        return this;
    }

    public String buttonTextColor() {
        return this.buttonTextColor;
    }

    public DataView buttonTextColor(String buttonTextColor) {
        this.buttonTextColor = buttonTextColor;
        return this;
    }

    public Boolean getReadOnly() {
        return readOnly;
    }

    public void setReadOnly(Boolean readOnly) {
        this.readOnly = readOnly;
    }

    public Object getOptionToContent() {
        return optionToContent;
    }

    public void setOptionToContent(Object optionToContent) {
        this.optionToContent = optionToContent;
    }

    public Object getContentToOption() {
        return contentToOption;
    }

    public void setContentToOption(Object contentToOption) {
        this.contentToOption = contentToOption;
    }

    public Object[] getLang() {
        return lang;
    }

    public void setLang(Object[] lang) {
        this.lang = lang;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getTextareaColor() {
        return textareaColor;
    }

    public void setTextareaColor(String textareaColor) {
        this.textareaColor = textareaColor;
    }

    public String getTextareaBorderColor() {
        return textareaBorderColor;
    }

    public void setTextareaBorderColor(String textareaBorderColor) {
        this.textareaBorderColor = textareaBorderColor;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public String getButtonColor() {
        return buttonColor;
    }

    public void setButtonColor(String buttonColor) {
        this.buttonColor = buttonColor;
    }

    public String getButtonTextColor() {
        return buttonTextColor;
    }

    public void setButtonTextColor(String buttonTextColor) {
        this.buttonTextColor = buttonTextColor;
    }
}
