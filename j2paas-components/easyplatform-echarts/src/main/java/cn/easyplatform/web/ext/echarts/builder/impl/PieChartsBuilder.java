/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.builder.impl;

import cn.easyplatform.web.ext.ComponentHandler;
import cn.easyplatform.web.ext.echarts.ECharts;

import java.util.*;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PieChartsBuilder extends AbstractChartsBuilder {

    public PieChartsBuilder(String type) {
        super(type);
    }

    @Override
    public void build(ECharts charts, ComponentHandler dataHandler) {
        if (dataHandler == null) {
            build(charts.getOption(), getTemplate(charts.getTemplate()), true);
        } else if (charts.getOption().getDataset() != null) {
            createDataset(charts, dataHandler);
        } else if (charts.getQuery() != null) {
            List<Object[]> data = dataHandler.selectList0(Object[].class, charts.getDbId(), (String) charts.getQuery());
            createModel(charts, data);
        } else {
            if (charts.getOption().getSeries() instanceof Map) {//单个饼图
                Map<String, Object> pie = (Map<String, Object>) charts.getOption().getSeries();
                if (pie.get("query") != null) {
                    List<Object[]> data = dataHandler.selectList0(Object[].class, charts.getDbId(), (String) pie.get("query"));
                    createModel(charts, data);
                }
            } else if (charts.getOption().getSeries() instanceof List) {
                List<?> series = (List<?>) charts.getOption().getSeries();
                if (series.size() == 1) {//单个饼图
                    Map<String, Object> pie = (Map<String, Object>) series.get(0);
                    if (pie.get("query") != null) {
                        List<Object[]> data = dataHandler.selectList0(Object[].class, charts.getDbId(), (String) pie.get("query"));
                        createModel(charts, data);
                    }
                } else {
                    //多个饼图
                    Set<Object> legend = new HashSet<>();
                    for (Object serie : series) {
                        Map<String, Object> pie = (Map<String, Object>) serie;
                        if (pie.get("query") != null) {
                            List<Object[]> data = dataHandler.selectList0(Object[].class, charts.getDbId(), (String) pie.remove("query"));
                            List<Object> model = new ArrayList<>();
                            for (Object[] objs : data) {
                                Map<String, Object> row = new HashMap<>();
                                row.put("name", objs[0]);
                                row.put("value", objs[1]);
                                model.add(row);
                                legend.add(objs[0]);
                            }
                            pie.put("data", model);
                        }
                    }
                    if (charts.getOption().getLegend() != null)
                        charts.getOption().getLegend().setData(legend);
                }
            }
        }
    }


    private void createModel(ECharts echarts, List<Object[]> model) {
        if (!model.isEmpty()) {
            Map<String, Object> pie = null;
            if (echarts.getOption().getSeries() instanceof Map)
                pie = (Map<String, Object>) echarts.getOption().getSeries();
            else if (echarts.getOption().getSeries() instanceof List)
                pie = (Map<String, Object>) ((List) echarts.getOption().getSeries()).get(0);
            List<Object> data = new ArrayList<>();
            List<Object> legend = new ArrayList<>();
            for (Object[] objs : model) {
                Map<String, Object> row = new HashMap<>();
                row.put("name", objs[0]);
                row.put("value", objs[1]);
                data.add(row);
                legend.add(objs[0]);
            }
            pie.put("data", data);
            if (echarts.getOption().getLegend() != null)
                echarts.getOption().getLegend().setData(legend);
        }else{
            Map<String, Object> pie = null;
            if (echarts.getOption().getSeries() instanceof Map)
                pie = (Map<String, Object>) echarts.getOption().getSeries();
            else if (echarts.getOption().getSeries() instanceof List)
                pie = (Map<String, Object>) ((List) echarts.getOption().getSeries()).get(0);
            pie.put("name","");
            Map<String, Object> normalMap = new HashMap<>();
            normalMap.put("show", true);
            Map<String, Object> labelMap = new HashMap<>();
            labelMap.put("normal", normalMap);
            pie.put("label",labelMap);
            List<Object> data = new ArrayList<>();
            Map<String, Object> dataMap = new HashMap<>();
            dataMap.put("name", "查无数据");
            dataMap.put("value", 0);
            data.add(dataMap);
            pie.put("data", data);
            echarts.clearInit();
        }
    }
}
