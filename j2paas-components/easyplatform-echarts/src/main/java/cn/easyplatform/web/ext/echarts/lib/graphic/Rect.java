/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.graphic;

import cn.easyplatform.web.ext.echarts.lib.graphic.style.GraphicStyle;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Rect extends Graphic {

    private Shape shape;

    private GraphicStyle style;

    public Rect() {
        this.type("rect");
    }

    public Shape shape() {
        if (shape == null)
            shape = new Shape();
        return shape;
    }

    public Rect shape(Shape shape) {
        this.shape = shape;
        return this;
    }

    public GraphicStyle style() {
        if (style == null)
            style = new GraphicStyle();
        return style;
    }

    public Rect style(GraphicStyle style) {
        this.style = style;
        return this;
    }

    public static class Shape {
        /**
         * 图形元素的左上角在父节点坐标系（以父节点左上角为原点）中的横坐标值
         */
        private Integer x;
        /**
         * 图形元素的左上角在父节点坐标系（以父节点左上角为原点）中的纵坐标值
         */
        private Integer y;
        /**
         * 图形元素的宽度
         */
        private Integer width;
        /**
         * 图形元素的高度
         */
        private Integer height;

        private Integer[] r;

        public Integer[] r() {
            return this.r;
        }

        public Shape r(Integer[] r) {
            this.r = r;
            return this;
        }

        public Integer x() {
            return this.x;
        }

        public Shape x(Integer x) {
            this.x = x;
            return this;
        }

        public Integer y() {
            return this.y;
        }

        public Shape y(Integer y) {
            this.y = y;
            return this;
        }

        public Integer width() {
            return this.width;
        }

        public Shape width(Integer width) {
            this.width = width;
            return this;
        }

        public Integer height() {
            return this.height;
        }

        public Shape height(Integer height) {
            this.height = height;
            return this;
        }

        public Integer getX() {
            return x;
        }

        public void setX(Integer x) {
            this.x = x;
        }

        public Integer getY() {
            return y;
        }

        public void setY(Integer y) {
            this.y = y;
        }

        public Integer getWidth() {
            return width;
        }

        public void setWidth(Integer width) {
            this.width = width;
        }

        public Integer getHeight() {
            return height;
        }

        public void setHeight(Integer height) {
            this.height = height;
        }

        public Integer[] getR() {
            return r;
        }

        public void setR(Integer[] r) {
            this.r = r;
        }
    }
}
