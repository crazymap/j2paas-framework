/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.axis;

import cn.easyplatform.web.ext.echarts.lib.style.LineStyle;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SplitLine implements Serializable {

    private static final long serialVersionUID = 6968396515815364363L;

    /**
     * 默认显示，属性show控制显示与否
     */
    private Boolean show;
    /**
     * 坐标轴分隔线的显示间隔，在类目轴中有效。默认同 axisLabel.interval 一样
     */
    private Object interval;
    /**
     * 分隔线线长。支持相对半径的百分比
     *
     * @Gauge
     */
    private Object length;

    /**
     * 属性lineStyle（详见lineStyle）控制线条样式
     *
     * @see LineStyle
     */
    private LineStyle lineStyle;

    public Boolean show() {
        return this.show;
    }

    public SplitLine show(Boolean show) {
        this.show = show;
        return this;
    }

    public Object interval() {
        return this.interval;
    }

    public SplitLine interval(Object interval) {
        this.interval = interval;
        return this;
    }

    public Object length() {
        return this.length;
    }

    public SplitLine length(Object length) {
        this.length = length;
        return this;
    }

    public SplitLine lineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
        return this;
    }

    public LineStyle lineStyle() {
        if (this.lineStyle == null) {
            this.lineStyle = new LineStyle();
        }
        return this.lineStyle;
    }

    public LineStyle getLineStyle() {
        return lineStyle;
    }

    public void setLineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
    }

    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public Object getInterval() {
        return interval;
    }

    public void setInterval(Object interval) {
        this.interval = interval;
    }

    public Object getLength() {
        return length;
    }

    public void setLength(Object length) {
        this.length = length;
    }
}
