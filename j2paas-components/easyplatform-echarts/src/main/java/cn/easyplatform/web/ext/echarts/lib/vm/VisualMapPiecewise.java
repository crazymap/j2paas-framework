/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.vm;

import cn.easyplatform.web.ext.echarts.lib.type.ItemSymbol;
import cn.easyplatform.web.ext.echarts.lib.type.SelectedMode;
import cn.easyplatform.web.ext.echarts.lib.type.VisualMapType;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class VisualMapPiecewise extends VisualMap {
    /**
     * 对于连续型数据，自动平均切分成几段。默认为5段。 连续数据的范围需要 max 和 min 来指定。
     */
    private Integer splitNumber;
    /**
     * 自定义『分段式视觉映射组件（visualMapPiecewise）』的每一段的范围，以及每一段的文字，以及每一段的特别的样式
     * pieces: [
     * {min: 1500}, // 不指定 max，表示 max 为无限大（Infinity）。
     * {min: 900, max: 1500},
     * {min: 310, max: 1000},
     * {min: 200, max: 300},
     * {min: 10, max: 200, label: '10 到 200（自定义label）'},
     * {value: 123, label: '123（自定义特殊颜色）', color: 'grey'}, // 表示 value 等于 123 的情况。
     * {max: 5}     // 不指定 min，表示 min 为无限大（-Infinity）。
     * ]
     */
    private Object pieces;
    /**
     * 用于表示离散型数据（或可以称为类别型数据、枚举型数据）的全集。
     * 当所指定的维度（visualMap-piecewise.dimension）的数据为离散型数据时，例如数据值为『优』、『良』等，那么可如下配置：
     * categories: ['严重污染', '重度污染', '中度污染', '轻度污染', '良', '优']
     */
    private Object categories;
    /**
     * 当 type 为 piecewise 且使用 min/max/splitNumber 时，此参数有效。当值为 true 时，界面上会额外多出一个『< min』的选块
     */
    private Boolean minOpen;
    /**
     * 当 type 为 piecewise 且使用 min/max/splitNumber 时，此参数有效。当值为 true 时，界面上会额外多出一个『> max』的选块。
     */
    private Boolean maxOpen;
    /**
     * 选择模式，可以是：
     * 'multiple'（多选）。
     * 'single'（单选）。
     *
     * @see SelectedMode
     */
    private String selectedMode;
    /**
     * 是否显示每项的文本标签。默认情况是，如果 visualMap-piecewise.text 被使用则不显示文本标签，否则显示
     */
    private Boolean showLabel;
    /**
     * 每两个图元之间的间隔距离，单位为px
     */
    private Integer itemGap;
    /**
     * 默认的图形。可选值为： 'circle', 'rect', 'roundRect', 'triangle', 'diamond', 'pin', 'arrow'
     *
     * @see ItemSymbol
     */
    private String itemSymbol;

    public VisualMapPiecewise() {
        this.setType(VisualMapType.piecewise.name());
    }

    public String itemSymbol() {
        return this.itemSymbol;
    }

    public VisualMapPiecewise itemSymbol(String itemSymbol) {
        this.itemSymbol = itemSymbol;
        return this;
    }

    public Integer itemGap() {
        return this.itemGap;
    }

    public VisualMapPiecewise itemGap(Integer itemGap) {
        this.itemGap = itemGap;
        return this;
    }

    public Integer splitNumber() {
        return this.splitNumber;
    }

    public VisualMapPiecewise splitNumber(Integer splitNumber) {
        this.splitNumber = splitNumber;
        return this;
    }

    public Object pieces() {
        return pieces;
    }

    public VisualMapPiecewise pieces(Object... pieces) {
        if (pieces.length == 0 || pieces[0] == null)
            return this;
        this.pieces = pieces;
        return this;
    }

    public Object categories() {
        return categories;
    }

    public VisualMapPiecewise categories(Object value) {
        if (value instanceof String)
            categories = ((String) value).split(";");
        else if (value instanceof Object[])
            this.categories = value;
        return this;
    }

    public Boolean minOpen() {
        return minOpen;
    }

    public VisualMapPiecewise minOpen(Boolean minOpen) {
        this.minOpen = minOpen;
        return this;
    }

    public Boolean maxOpen() {
        return maxOpen;
    }

    public VisualMapPiecewise maxOpen(Boolean maxOpen) {
        this.maxOpen = maxOpen;
        return this;
    }

    public String selectedMode() {
        return selectedMode;
    }

    public VisualMapPiecewise selectedMode(String selectedMode) {
        this.selectedMode = selectedMode;
        return this;
    }

    public Boolean showLabel() {
        return showLabel;
    }

    public VisualMapPiecewise showLabel(Boolean showLabel) {
        this.showLabel = showLabel;
        return this;
    }
}