/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Dataset implements Serializable {
    /**
     * 原始数据。一般来说，原始数据表达的是二维表
     */
    private Object source;
    /**
     * 使用 dimensions 定义 series.data 或者 dataset.source 的每个维度的信息。
     */
    private Object dimensions;
    /**
     * dataset.source 第一行/列是否是 维度名 信息。注意：“第一行/列” 的意思是，如果 series.seriesLayoutBy 设置为 'column'（默认值），则取第一行，如果 series.seriesLayoutBy 设置为 'row'，则取第一列。
     */
    private Boolean sourceHeader;

    public Dataset() {
    }

    public Dataset(Object source) {
        this.source = source;
    }

    public Boolean sourceHeader() {
        return this.sourceHeader;
    }

    public Dataset sourceHeader(Boolean sourceHeader) {
        this.sourceHeader = sourceHeader;
        return this;
    }

    public Object dimensions() {
        return this.dimensions;
    }

    public Dataset dimensions(Object dimensions) {
        this.dimensions = dimensions;
        return this;
    }

    public Object source() {
        return this.source;
    }

    public Dataset source(Object source) {
        this.source = source;
        return this;
    }

    public Object getSource() {
        return source;
    }

    public void setSource(Object source) {
        this.source = source;
    }

    public Object getDimensions() {
        return dimensions;
    }

    public void setDimensions(Object dimensions) {
        this.dimensions = dimensions;
    }

    public Boolean getSourceHeader() {
        return sourceHeader;
    }

    public void setSourceHeader(Boolean sourceHeader) {
        this.sourceHeader = sourceHeader;
    }
}
