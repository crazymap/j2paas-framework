/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.zul;

import cn.easyplatform.web.ext.Assignable;
import cn.easyplatform.web.ext.ZkExt;
import org.zkoss.zul.Vlayout;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Explorer extends Vlayout implements ZkExt, Assignable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 源目录
	 */
	private String _src;

	/**
	 * 文件排列的列数
	 */
	private int _cols;

	/**
	 * 是否显示工具栏
	 */
	private boolean _toolbar = true;

	/**
	 * 是否允许上传文件
	 */
	private boolean _upload;

	/**
	 * 最大的上传文件大小,单位kb,-1表示没有限制
	 */
	private int _maxSize = -1;

	/**
	 * 是否可以多个文件上传
	 */
	private boolean _multiple;

	/**
	 * 能够被提交或上传的一个或多个 MIME 类型，如需规定多个 MIME
	 * 类型，请使用逗号分隔这些类型,例如：audio/*|video/*|image
	 */
	private String _accept;

	/**
	 * 是否允许下载文件
	 */
	private boolean _download;

	/**
	 * 预览
	 */
	private boolean _preview;

	/**
	 * 是否可删除文件
	 */
	private boolean _delete;

	/**
	 * 是否可以创建目录
	 */
	private boolean _mkdir;

	/**
	 * 是否显示文件夹
	 */
	private boolean _folder;

	/**
	 * 当src不存在时是否创建目录
	 */
	private boolean _force;

	public boolean isPreview() {
		return _preview;
	}

	public void setPreview(boolean preview) {
		this._preview = preview;
	}

	public boolean isForce() {
		return _force;
	}

	public void setForce(boolean force) {
		this._force = force;
	}

	/**
	 * @return the maxSize
	 */
	public int getMaxSize() {
		return _maxSize;
	}

	/**
	 * @param maxSize
	 *            the maxSize to set
	 */
	public void setMaxSize(int maxSize) {
		this._maxSize = maxSize;
	}

	/**
	 * @return the multiple
	 */
	public boolean isMultiple() {
		return _multiple;
	}

	/**
	 * @param multiple
	 *            the multiple to set
	 */
	public void setMultiple(boolean multiple) {
		this._multiple = multiple;
	}

	/**
	 * @return the accept
	 */
	public String getAccept() {
		return _accept;
	}

	/**
	 * @param accept
	 *            the accept to set
	 */
	public void setAccept(String accept) {
		this._accept = accept;
	}

	public boolean isFolder() {
		return _folder;
	}

	public void setFolder(boolean folder) {
		this._folder = folder;
	}

	public boolean isToolbar() {
		return _toolbar;
	}

	public void setToolbar(boolean toolbar) {
		this._toolbar = toolbar;
	}

	public String getSrc() {
		return _src;
	}

	public void setSrc(String src) {
		this._src = src;
	}

	public int getCols() {
		return _cols;
	}

	public void setCols(int cols) {
		this._cols = cols;
	}

	public boolean isUpload() {
		return _upload;
	}

	public void setUpload(boolean upload) {
		this._upload = upload;
	}

	public boolean isDownload() {
		return _download;
	}

	public void setDownload(boolean download) {
		this._download = download;
	}

	public boolean isDelete() {
		return _delete;
	}

	public void setDelete(boolean delete) {
		this._delete = delete;
	}

	public boolean isMkdir() {
		return _mkdir;
	}

	public void setMkdir(boolean mkdir) {
		this._mkdir = mkdir;
	}

	@Override
	public void setValue(Object value) {
		Assignable ext = (Assignable) getAttribute("$proxy");
		if (ext != null)
			ext.setValue(value);
	}

	@Override
	public Object getValue() {
		Assignable ext = (Assignable) getAttribute("$proxy");
		if (ext != null)
			return ext.getValue();
		return null;
	}

}
