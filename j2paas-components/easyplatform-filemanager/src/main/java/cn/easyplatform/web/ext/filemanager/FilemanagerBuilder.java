/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.filemanager;

import cn.easyplatform.web.ext.ComponentBuilder;
import cn.easyplatform.web.ext.ComponentHandler;
import cn.easyplatform.web.ext.Widget;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Window;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FilemanagerBuilder implements ComponentBuilder, Widget {

    private ComponentHandler dataHandler;

    private Filemanager filemanager;

    public FilemanagerBuilder(ComponentHandler dataHandler, Filemanager filemanager) {
        this.dataHandler = dataHandler;
        this.filemanager = filemanager;
    }


    public Component build() {
        filemanager.setAttribute("$proxy", this);
        create(filemanager,true);
        return filemanager;
    }


    private void create(Filemanager cbx,boolean addEvtFlag) {

        String userId=dataHandler.getUserId();
        String userQuery = "select userId,name from sys_user_info where userId <> '"+userId+"'";
        List<?> userList = dataHandler.selectList0(Object[].class, cbx.getDbId(), userQuery);

        Map<String, Object> args = new HashMap<>();
        args.put("filemanager", filemanager);
        args.put("userList", userList);
        args.put("userId", userId);
        args.put("componentHandler",dataHandler);
        Window win = (Window) Executions.createComponents("~./js/filemanager/builder/builder.zul", null, args);
        win.setPage(cbx.getPage());
        win.setPosition("top,center");
        win.doOverlapped();
        if(cbx.getEvent()!=null && cbx.getEvent()!="" && addEvtFlag==true) {
            dataHandler.addEventListener("onLink", cbx);
        }
    }


    public void reload(Component widget) {
        Filemanager cbx = (Filemanager)widget;
        this.create(cbx,false);
        cbx.setReloadFlag(true);
    }

}
