/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.filemanager;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.web.annotation.Builder;
import cn.easyplatform.web.ext.Reloadable;
import cn.easyplatform.web.ext.Widget;
import cn.easyplatform.web.ext.ZkExt;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import org.zkoss.zk.au.AuRequest;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.impl.XulElement;

import java.util.Map;

@Builder(FilemanagerBuilder.class)
public class Filemanager extends XulElement implements ZkExt, Reloadable {

	public final static String ON_Link = "onLink";

	static {
		addClientEvent(Filemanager.class, ON_Link, CE_IMPORTANT);
	}

	private String[] ontactInfo;

	private boolean reloadFlag;

	private String privatePath;
	private String groupPath;
	private String dbId;
	private String filter;


	public String getPersonPath() {
		return privatePath;
	}

	public void setPrivatePath(String personPath) {
		if (!Strings.isBlank(personPath)) {
			this.privatePath = personPath;
			smartUpdate("privatePath", this.privatePath);
		}
	}

	public String getGroupPath() {
		return groupPath;
	}

	public void setGroupPath(String groupPath) {
		if (!Strings.isBlank(groupPath)) {
			this.groupPath = groupPath;
			smartUpdate("groupPath", this.groupPath);
		}
	}

	public String getDbId() {
		return dbId;
	}

	public void setDbId(String dbId) {
		if (!Strings.isBlank(dbId)) {
			this.dbId = dbId;
			smartUpdate("dbId", this.dbId);
		}
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		if (!Strings.isBlank(filter)) {
			this.filter = filter;
			smartUpdate("filter", this.filter);
		}
	}

	public String[] getOntactInfo() {
		return ontactInfo;
	}

	public void setOntactInfo(String ontactInfo) {
		if (!Strings.isBlank(ontactInfo)) {
			try {
				JSONArray js = JSON.parseArray(ontactInfo);
				this.ontactInfo = js.toArray(new String[]{});
			} catch (Exception e) {
				e.printStackTrace();
			}
			smartUpdate("ontactInfo", this.ontactInfo);
		}
	}

	public boolean isReloadFlag() {
		return reloadFlag;
	}

	public void setReloadFlag(boolean reloadFlag) {

		/*FilemanagerBuilder a = new FilemanagerBuilder(new ComponentHandler() {
			@Override
			public <T> List<T> selectList(Class<T> aClass, String s, Object... objects) {
				return null;
			}

			@Override
			public <T> List<T> selectList(Class<T> aClass, String s, String s1, Object... objects) {
				return null;
			}

			@Override
			public <T> List<T> selectList(Class<T> aClass, String s, int i, int i1, boolean b, Object... objects) {
				return null;
			}

			@Override
			public <T> List<T> selectList(Class<T> aClass, String s, String s1, int i, int i1, boolean b, Object... objects) {
				return null;
			}

			@Override
			public <T> T selectOne(Class<T> aClass, String s, Object... objects) {
				return null;
			}

			@Override
			public <T> T selectOne(Class<T> aClass, String s, String s1, Object... objects) {
				return null;
			}

			@Override
			public Object selectObject(String s, Object... objects) {
				return null;
			}

			@Override
			public Object selectObject(String s, String s1, Object... objects) {
				return null;
			}

			@Override
			public int executeUpdate(String s, Object... objects) {
				return 0;
			}

			@Override
			public int executeUpdate(String s, String s1, Object... objects) {
				return 0;
			}

			@Override
			public Object getVariable(String s) {
				return null;
			}

			@Override
			public void addEventListener(String s, Component component) {

			}

			@Override
			public void addEventListener(String s, Component component, String s1) {

			}

			@Override
			public String getUserId() {
				return null;
			}

			@Override
			public String getProjectId() {
				return null;
			}
		},this);
		a.build();*/

		this.reloadFlag = reloadFlag;
		smartUpdate("reloadFlag", this.reloadFlag);
	}

	@Override
	public boolean isForce() {
		return false;
	}


	//super//
	protected void renderProperties(org.zkoss.zk.ui.sys.ContentRenderer renderer)
	throws java.io.IOException {
		super.renderProperties(renderer);

		render(renderer, "ontactInfo", ontactInfo);
		render(renderer, "reloadFlag", reloadFlag);
	}
	
	public void service(AuRequest request, boolean everError) {
		final String cmd = request.getCommand();
		final Map data = request.getData();

		if (cmd.equals(ON_Link)) {
			Event evt = new Event(cmd, this, request.getData().get("data"));
			Events.postEvent(evt);
		} else
			super.service(request, everError);
	}

	/**
	 * The default zclass is "z-filemanager"
	 */
	public String getZclass() {
		return (this._zclass != null ? this._zclass : "z-filemanager");
	}

	public void clear() {
		Clients.evalJavaScript("zk.$('" + this.getUuid() + "').allClear()");
	}

	@Override
	public void reload() {
		Widget ext = (Widget) getAttribute("$proxy");
		ext.reload(this);
	}

}

