/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.layout.menu;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.vos.*;
import cn.easyplatform.web.layout.IMenuBuilder;
import cn.easyplatform.web.listener.RunTaskListener;
import cn.easyplatform.web.utils.ExtUtils;
import cn.easyplatform.web.utils.PageUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Menu;
import org.zkoss.zul.Menubar;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Menupopup;

import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MenuBarMenuBuilder implements IMenuBuilder {

    private Menubar menubar;

    private Component container;

    /**
     * @param menubar
     */
    public MenuBarMenuBuilder(Menubar menubar) {
        this.menubar = menubar;
    }

    @Override
    public void fill(AuthorizationVo av) {
        if (av == null || av.getRoles() == null)
            return;
        this.container = menubar.getFellow("gv5container");
        for (RoleVo rv : av.getRoles())
            createMenu(menubar, rv, null);
        if (av.getAgents() != null && !av.getAgents().isEmpty()) {
            for (AgentVo agent : av.getAgents()) {
                for (RoleVo rv : agent.getRoles())
                    createMenu(menubar, rv, agent);
            }
        }
    }

    private void createMenu(Component menubar, RoleVo rv, AgentVo av) {
        for (MenuVo mv : rv.getMenus()) {
            Menu menu = new Menu(mv.getName());
            Menupopup mp = new Menupopup();
            if (!Strings.isBlank(mv.getImage()))
                menu.setIconSclass(mv.getImage());
            else
                menu.setIconSclass(ExtUtils.getIconSclass());
            mp.setParent(menu);
            menu.setParent(menubar);
            if (mv.getChildMenus() != null && !mv.getChildMenus().isEmpty()) {
                createMenu(mp, mv.getChildMenus(), rv, av);
            } else if (mv.getTasks() != null && !mv.getTasks().isEmpty()) {
                createTask(mp, mv.getTasks(), rv, av);
            }
        }
    }

    private void createMenu(Component parent, List<MenuVo> menus, RoleVo rv,
                            AgentVo av) {
        for (MenuVo mv : menus) {
            Menu menu = new Menu(mv.getName());
            Menupopup mp = new Menupopup();
            if (!Strings.isBlank(mv.getImage()))
                menu.setIconSclass(mv.getImage());
            else
                menu.setIconSclass("z-icon-chevron-right z-menu-image");
            if (mv.getChildMenus() != null && !mv.getChildMenus().isEmpty())
                createMenu(mp, mv.getChildMenus(), rv, av);
            if (mv.getTasks() != null && !mv.getTasks().isEmpty())
                createTask(mp, mv.getTasks(), rv, av);
            mp.setParent(menu);
            menu.setParent(parent);
        }
    }

    private void createTask(Component parent, List<MenuNodeVo> tasks,
                            RoleVo rv, AgentVo av) {
        for (MenuNodeVo tv : tasks) {
            tv.setRoleId(rv.getId());
            if (av != null)
                tv.setAgent(av.getId());
            Menuitem nv = new Menuitem();
            nv.setLabel(tv.getName());
            nv.setTooltiptext(tv.getDesp() == null ? tv.getName() : tv
                    .getDesp());
            PageUtils.setTaskIcon(nv, tv.getImage());
            nv.setParent(parent);
            nv.addEventListener(Events.ON_CLICK, new RunTaskListener(tv, container));
        }
    }
}
