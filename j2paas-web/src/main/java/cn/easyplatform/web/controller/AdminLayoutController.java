/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.controller;

import cn.easyplatform.messages.vos.AuthorizationVo;
import cn.easyplatform.messages.vos.EnvVo;
import cn.easyplatform.web.WebApps;
import cn.easyplatform.web.contexts.Contexts;
import cn.easyplatform.web.layout.menu.NavbarMenuBuilder;
import cn.easyplatform.web.listener.SessionValidationScheduler;
import cn.easyplatform.web.task.OperableHandler;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zkmax.zul.Navbar;
import org.zkoss.zul.*;
import org.zkoss.zul.impl.LabelImageElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class AdminLayoutController extends AbstractLayoutController {

    private Tabbox tabbox;

    @Override
    protected void init(Component comp) {
        tabbox = (Tabbox) comp.getFellow("gv5container");
        SessionValidationScheduler.start(comp);
        WebApps.me().subscribe(comp.getDesktop());
    }

    public void menu(Navbar navbar) {
        AuthorizationVo av = Contexts.getUserAuthorzation();
        if (av == null || av.getRoles() == null)
            return;
        new NavbarMenuBuilder(navbar).fill(av);
    }

    public void go(String taskId) {
        for (Component comp : tabbox.getTabs().getChildren()) {
            Tab tab = (Tab) comp;
            if (tab.getValue() instanceof OperableHandler) {
                OperableHandler rc = tab.getValue();
                if (taskId.equals(rc.getTaskId())) {
                    tab.setSelected(true);
                    return;
                }
            }
        }
        doTask(tabbox, taskId, false, null, null, "default");
    }

    /**
     * 设定系统的标题
     *
     * @param c
     */
    public void title(Component c) {
        EnvVo env = Contexts.getEnv();
        if (c instanceof Label) {
            Label header = (Label) c;
            header.setValue(env.getName());
        } else if (c instanceof A) {
            A a = (A) c;
            a.setLabel(env.getName());
        }
    }

    /**
     * 创建模版
     *
     * @param nav
     * @return
     */
    private Tabpanel createPanel(LabelImageElement nav) {
        for (Component c : tabbox.getTabs().getChildren()) {
            Tab tab = (Tab) c;
            if (tab.getLabel().equals(nav.getLabel())) {
                tabbox.setSelectedTab(tab);
                return null;
            }
        }
        Tab tab = new Tab(nav.getLabel());
        tab.setSelected(true);
        tab.setClosable(true);
        tab.setIconSclass(nav.getIconSclass());
        tab.setParent(tabbox.getTabs());
        Tabpanel panel = new Tabpanel();
        panel.setParent(tabbox.getTabpanels());
        return panel;
    }

    public void showConfig(LabelImageElement nav, String name) {
        Tabpanel container = createPanel(nav);
        if (container != null) {
            Idspace is = new Idspace();
            is.setParent(container);
            is.setWidth("100%");
            is.setHeight("100%");
            Map<String, Object> args = null;
            if (name.equals("oe") || name.equals("le")) {//op event or login event
                args = new HashMap<>();
                args.put("type", name);
                name = "eventlog";
            }
            Executions.createComponents("~./admin/" + name + ".zul", is, args);
        }
    }

    public void close(Component ref, int type) {
        List<Tab> tabs = tabbox.getTabs().getChildren();
        List<Tab> closableTabs = new ArrayList<Tab>();
        for (Tab tab : tabs) {
            if (type == 0) {// 关闭当前功能
                if (tab.isSelected()) {
                    closableTabs.add(tab);
                    break;
                }
            } else if (type == 1) {// 关闭其它功能
                if (!tab.isSelected())
                    closableTabs.add(tab);
            } else {// 关闭所有功能
                closableTabs.add(tab);
            }
        }
        for (Tab tab : closableTabs)
            tab.close();
    }
}
