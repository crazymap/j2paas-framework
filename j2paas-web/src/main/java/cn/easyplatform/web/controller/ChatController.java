/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.controller;

import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.vos.SqlVo;
import cn.easyplatform.messages.vos.h5.MessageVo;
import cn.easyplatform.spi.service.AddonService;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.web.contexts.Contexts;
import cn.easyplatform.web.service.ServiceLocator;
import com.alibaba.fastjson.JSONObject;
import org.zkoss.xel.VariableResolver;
import org.zkoss.xel.util.SimpleResolver;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Window;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author Zeta
 * @Version 1.0
 */

public class ChatController extends SelectorComposer<Component> implements EventListener<Event> {

    private Div chat_div_close;

    private Window window;

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
                                         ComponentInfo compInfo)  {


        List friendList = new ArrayList();
        List groupList = new ArrayList();

        AddonService as = ServiceLocator.lookup(AddonService.class);
        SqlVo vo = new SqlVo();
        vo.setGetFieldName(true);
        vo.setQuery("select a.userid,b.name,b.head_url from group_chat_info a left join " +
                "sys_user_info b on a.userid=b.userId order by a.type desc");//查询好友
        IResponseMessage<?> friendResp = as.selectList(new SimpleRequestMessage(vo));
        if (friendResp.isSuccess()) {
            if(friendResp.getBody() instanceof List) {
                friendList = (List) friendResp.getBody();
            }
        }
        vo.setQuery("select b.name,a.create_date from group_chat_info a left join group_chat b" +
                " on a.groupid=b.id where a.userid='"+Contexts.getUser().getId()+"'");//查询群组
        IResponseMessage<?> groupResp = as.selectList(new SimpleRequestMessage(vo));
        if (groupResp.isSuccess()) {
            if(groupResp.getBody() instanceof List) {
                groupList = (List) groupResp.getBody();
            }
        }
        JSONObject text = new JSONObject();
        text.put("num","a");
        JSONObject text2 = new JSONObject();
        text2.put("num","b");
        List numList = new ArrayList();
        numList.add(text);
        numList.add(text2);
        page.setAttribute("friendItems",friendList);
        page.setAttribute("groupItems",groupList);
        page.setAttribute("numListItems",numList);

        /*Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("$param", object);
        VariableResolver variableResolver = new SimpleResolver(variables);
        page.addVariableResolver(variableResolver);*/

        return compInfo;

    }

    @Override
    public void doAfterCompose(final Component comp) throws Exception {

        window = (Window)comp.getParent().getParent();
        chat_div_close= (Div)comp.getFellow("chat_div_close");
        chat_div_close.addEventListener(Events.ON_CLICK, this);

    }


    @Override
    public void onEvent(Event event) throws Exception {

        if (event.getTarget().equals(chat_div_close)) {
            window.detach();
        }else if(event.getTarget().equals("")){

        }
    }



}
