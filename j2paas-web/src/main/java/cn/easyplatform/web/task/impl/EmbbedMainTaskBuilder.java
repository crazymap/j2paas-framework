/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.impl;

import cn.easyplatform.messages.vos.AbstractPageVo;
import cn.easyplatform.web.utils.WebUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Idspace;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class EmbbedMainTaskBuilder extends AbstractMainTaskBuilder<Idspace> {

    /**
     * @param container
     * @param taskId
     * @param pv
     */
    public EmbbedMainTaskBuilder(Component container, String taskId,
                                 AbstractPageVo pv) {
        super(container, taskId, pv);
    }

    @Override
    public void build() {
        container.getChildren().clear();
        clear();
        super.build();
        container.appendChild(idSpace);
    }

    @Override
    public void reload(String... comps) {
//        if (container instanceof Taskbox)
//            ((Taskbox) container).reload();
//        else
            super.reload(comps);
    }

    @Override
    public void close(boolean normal) {
        WebUtils.removeTask(getId());
        clear();
        idSpace.detach();
        idSpace = null;
    }
}
