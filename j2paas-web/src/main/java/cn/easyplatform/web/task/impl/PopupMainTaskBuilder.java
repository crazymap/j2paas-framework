/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.impl;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.vos.AbstractPageVo;
import cn.easyplatform.messages.vos.VisibleVo;
import cn.easyplatform.spi.service.TaskService;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.web.dialog.MessageBox;
import cn.easyplatform.web.service.ServiceLocator;
import cn.easyplatform.web.utils.WebUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Idspace;
import org.zkoss.zul.Popup;
import org.zkoss.zul.impl.XulElement;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PopupMainTaskBuilder extends AbstractMainTaskBuilder<Idspace> {

    private Popup panel;

    public PopupMainTaskBuilder(Component container, String taskId, AbstractPageVo apv) {
        super(container, taskId, apv);
    }


    @Override
    public void build() {
        XulElement c = (XulElement) this.container;
        if (c.hasAttribute("uuid")) {
            String uuid = (String) c.getAttribute("uuid");
            ServiceLocator.lookup(
                    TaskService.class).close(
                    new SimpleRequestMessage(uuid, null));
            WebUtils.removeTask(getId());
            ((Popup) c.getAttribute("popup")).detach();
        }
        panel = new Popup();
        super.build();
        panel.appendChild(idSpace);
        panel.setPage(c.getPage());
        setSize();
        c.setAttribute("uuid", getId());
        c.setAttribute("popup", panel);
        String position = ((VisibleVo) apv).getPosition();
        if (Strings.isBlank(position))
            position = "after_center";
        panel.open(c, position);
    }

    @Override
    protected void doPage() {
        super.doPage();
        setSize();
    }

    private void setSize() {
        VisibleVo vv = (VisibleVo) apv;
        if (vv.getHeight() > 0)
            panel.setHeight(vv.getHeight() + "px");
        else
            panel.setHeight("650px");
        if (vv.getWidth() > 0)
            panel.setWidth(vv.getWidth() + "px");
        else
            panel.setWidth("800px");
    }

    @Override
    public void close(boolean normal) {
        if (!normal) {
            IResponseMessage<?> resp = ServiceLocator.lookup(
                    TaskService.class).close(
                    new SimpleRequestMessage(getId(), null));
            if (!resp.isSuccess()) {
                MessageBox.showMessage(resp);
                return;
            }
        }
        WebUtils.removeTask(getId());
        clear();
        idSpace.detach();
        idSpace = null;
        panel.detach();
        panel = null;
    }
}
