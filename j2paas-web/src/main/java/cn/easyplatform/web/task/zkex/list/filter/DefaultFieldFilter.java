/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.zkex.list.filter;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.vos.datalist.ListHeaderVo;
import cn.easyplatform.messages.vos.datalist.ListQueryParameterVo;
import cn.easyplatform.type.FieldType;
import cn.easyplatform.web.dialog.MessageBox;
import cn.easyplatform.web.ext.zul.ComboboxExt;
import cn.easyplatform.web.task.zkex.ListSupport;
import cn.easyplatform.web.task.BackendException;
import cn.easyplatform.web.utils.PageUtils;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;
import org.zkoss.zul.impl.XulElement;

import java.util.HashMap;
import java.util.Map;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DefaultFieldFilter implements FieldFilter, EventListener<Event> {

    private ListSupport dls;

    private ListHeaderVo hv;

    private ComboboxExt op;

    private XulElement v1;

    private XulElement v2;

    @Override
    public void doFilter(ListHeaderVo hv, ListSupport dls) {
        this.hv = hv;
        this.dls = dls;
        Window win = new Window();
        win.setPage(dls.getComponent().getPage());
        win.setTitle(Labels.getLabel("datalist.filter.title"));
        win.setClosable(true);
        win.setSizable(true);
        win.setBorder(true);
        win.setPosition("center");
        win.setShadow(true);
        createContent(win);
        win.doOverlapped();
    }

    private void createContent(Component win) {

        op = new ComboboxExt();
        op.setWidth("80px");
        Comboitem ci = new Comboitem();
        ci.setLabel(Labels.getLabel("datalist.query.op.eq"));
        ci.setValue("=");
        ci.setParent(op);

        ci = new Comboitem();
        ci.setLabel(Labels.getLabel("datalist.query.op.ne"));
        ci.setValue("<>");
        ci.setParent(op);

        Borderlayout layout = new Borderlayout();
        layout.setHeight("70px");
        Center center = new Center();
        center.setBorder("none");
        center.setHflex("1");
        center.setParent(layout);
        Hlayout container = new Hlayout();
        container.setValign("middle");
        container.setHflex("1");
        container.appendChild(new Label(Labels.getLabel("datalist.query.op")
                + ":"));
        container.appendChild(op);
        container.appendChild(new Label(hv.getTitle() + ":"));
        container.setParent(center);
        layout.setParent(win);

        if (hv.getType() == FieldType.CHAR || hv.getType() == FieldType.VARCHAR
                || hv.getType() == FieldType.BLOB) {
            ci = new Comboitem();
            ci.setLabel(Labels.getLabel("datalist.query.op.li"));
            ci.setValue("like");
            ci.setParent(op);

            ci = new Comboitem();
            ci.setLabel(Labels.getLabel("datalist.query.op.ls"));
            ci.setValue("ls");
            ci.setParent(op);

            ci = new Comboitem();
            ci.setLabel(Labels.getLabel("datalist.query.op.lf"));
            ci.setValue("lf");
            ci.setParent(op);
            v1 = createInputComponent(hv.getType());
            v1.setWidth("100px");
            v1.addEventListener(Events.ON_OK, this);
            container.appendChild(v1);
            layout.setWidth("330px");
        } else if (hv.getType() == FieldType.INT
                || hv.getType() == FieldType.LONG
                || hv.getType() == FieldType.NUMERIC
                || hv.getType() == FieldType.DATETIME
                || hv.getType() == FieldType.DATE
                || hv.getType() == FieldType.TIME) {
            ci = new Comboitem();
            ci.setLabel(Labels.getLabel("datalist.query.op.ge"));
            ci.setValue(">=");
            ci.setParent(op);

            ci = new Comboitem();
            ci.setLabel(Labels.getLabel("datalist.query.op.le"));
            ci.setValue("<=");
            ci.setParent(op);

            ci = new Comboitem();
            ci.setLabel(Labels.getLabel("datalist.query.op.between"));
            ci.setValue("between");
            ci.setParent(op);

            ci = new Comboitem();
            ci.setLabel(Labels.getLabel("datalist.query.op.nbetween"));
            ci.setValue("not between");
            ci.setParent(op);
            v1 = createInputComponent(hv.getType());
            v1.setWidth("100px");
            v1.addEventListener(Events.ON_OK, this);
            container.appendChild(v1);
            Div div = new Div();
            div.setHflex("1");
            Label sep = new Label("-");
            sep.setStyle("margin-right:5px");
            div.appendChild(sep);
            v2 = createInputComponent(hv.getType());
            v2.setWidth("100px");
            v2.addEventListener(Events.ON_OK, this);
            div.appendChild(v2);
            container.appendChild(div);
            if (hv.getType() == FieldType.DATETIME
                    || hv.getType() == FieldType.DATE
                    || hv.getType() == FieldType.TIME)
                layout.setWidth("395px");
            else
                layout.setWidth("375px");
        } else {
            v1 = createInputComponent(hv.getType());
            container.appendChild(v1);
            layout.setWidth("300px");
        }
        South south = new South();
        south.setSclass("text-center");
        Button button = new Button();
        button.setWidth("100px");
        button.setLabel(Labels.getLabel("button.ok"));
        button.setParent(south);
        button.addEventListener(Events.ON_CLICK, this);
        south.setParent(layout);
        south.setBorder("none");
        op.setReadonly(true);
        op.setSelectedIndex(0);
    }

    @Override
    public void onEvent(Event event) throws Exception {
        Map<String, ListQueryParameterVo> params = new HashMap<String, ListQueryParameterVo>();
        String o = op.getSelectedItem().getValue();
        ListQueryParameterVo qpv = new ListQueryParameterVo(hv.getField(), o,
                null);
        Object value = PageUtils.getValue(v1, true);
        if (value != null && !Strings.isBlank(value.toString())) {
            qpv.setValue(value);
            params.put(hv.getField(), qpv);
            if (v2 != null) {
                value = PageUtils.getValue(v2, true);
                if (value != null && !Strings.isBlank(value.toString()))
                    qpv.setValue(value);
            }
        }
        try {
            dls.query(params);
            event.getTarget().getRoot().detach();
        } catch (BackendException e) {
            MessageBox.showMessage(e.getMsg());
        }
    }

    private XulElement createInputComponent(FieldType type) {
        if (type == FieldType.CHAR || type == FieldType.VARCHAR
                || type == FieldType.BLOB)
            return new Textbox();
        else if (type == FieldType.BOOLEAN)
            return new Checkbox();
        else if (type == FieldType.DATETIME || type == FieldType.DATE)
            return new Datebox();
        else if (type == FieldType.TIME)
            return new Timebox();
        else if (type == FieldType.INT)
            return new Intbox();
        else if (type == FieldType.LONG)
            return new Longbox();
        else
            return new Doublebox();
    }
}
