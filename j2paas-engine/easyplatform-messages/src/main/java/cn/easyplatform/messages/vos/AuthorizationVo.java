/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos;

import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class AuthorizationVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String orgId;
	
	private String orgName;
	
	private String mainPage;

	private List<RoleVo> roles;

	private List<AgentVo> agents;

	/**
	 * @param mainPage
	 * @param roles
	 */
	public AuthorizationVo(String mainPage, List<RoleVo> roles) {
		this.mainPage = mainPage;
		this.roles = roles;
	}

	/**
	 * @return the agents
	 */
	public List<AgentVo> getAgents() {
		return agents;
	}

	/**
	 * @param agents
	 *            the agents to set
	 */
	public void setAgents(List<AgentVo> agents) {
		this.agents = agents;
	}

	/**
	 * @return the roles
	 */
	public List<RoleVo> getRoles() {
		return roles;
	}

	/**
	 * @param roles
	 */
	public void setRoles(List<RoleVo> roles) {
		this.roles = roles;
	}

	/**
	 * @return the mainPage
	 */
	public String getMainPage() {
		return mainPage;
	}

	/**
	 * @return
	 */
	public String getOrgId() {
		return orgId;
	}

	/**
	 * @param orgId
	 */
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

}
