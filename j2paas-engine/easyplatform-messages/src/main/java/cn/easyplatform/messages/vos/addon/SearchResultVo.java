/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.addon;

import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SearchResultVo implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//总笔数
	private int totalSize;
	
	//查询数据
	private List<String[]> data;

	/**
	 * @param totalSize
	 * @param data
	 */
	public SearchResultVo(int totalSize, List<String[]> data) {
		this.totalSize = totalSize;
		this.data = data;
	}

	/**
	 * @param totalSize
	 * @param data
	 */
	public SearchResultVo(List<String[]> data) {
		this.data = data;
	}

	/**
	 * @return the totalSize
	 */
	public int getTotalSize() {
		return totalSize;
	}

	/**
	 * @return the data
	 */
	public List<String[]> getData() {
		return data;
	}	
	
}
