/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.admin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class CustomVo implements Serializable {

    private int type;

    private List<Entry> entries;

    public CustomVo(int type) {
        this.type = type;
    }

    public void addEntry(Entry entry) {
        if (entries == null)
            entries = new ArrayList<>();
        entries.add(entry);
    }

    public int getType() {
        return type;
    }

    public List<Entry> getEntries() {
        return entries;
    }

    public static class Entry implements Serializable {
        private String name;
        private String className;
        private String description;
        private String content;
        private char flag = 'R';

        public Entry(String name, String className, String description) {
            this.name = name;
            this.className = className;
            this.description = description;
        }

        public Entry(String className, String description) {
            this.className = className;
            this.description = description;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getName() {
            return name;
        }

        public String getClassName() {
            return className;
        }

        public String getDescription() {
            return description;
        }

        public char getFlag() {
            return flag;
        }

        public void setFlag(char flag) {
            this.flag = flag;
        }
    }
}
