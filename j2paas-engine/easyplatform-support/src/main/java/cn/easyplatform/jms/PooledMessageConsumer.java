/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PooledMessageConsumer implements MessageConsumer {

    private final PooledSession session;
    private final MessageConsumer delegate;

    public PooledMessageConsumer(PooledSession session, MessageConsumer delegate) {
        this.session = session;
        this.delegate = delegate;
    }

    @Override
    public void close() throws JMSException {
        session.onConsumerClose(delegate);
        delegate.close();
    }

    @Override
    public MessageListener getMessageListener() throws JMSException {
        return delegate.getMessageListener();
    }

    @Override
    public String getMessageSelector() throws JMSException {
        return delegate.getMessageSelector();
    }

    @Override
    public Message receive() throws JMSException {
        return delegate.receive();
    }

    @Override
    public Message receive(long timeout) throws JMSException {
        return delegate.receive(timeout);
    }

    @Override
    public Message receiveNoWait() throws JMSException {
        return delegate.receiveNoWait();
    }

    @Override
    public void setMessageListener(MessageListener listener) throws JMSException {
        delegate.setMessageListener(listener);
    }

    @Override
    public String toString() {
        return "PooledMessageConsumer { " + delegate + " }";
    }
}
