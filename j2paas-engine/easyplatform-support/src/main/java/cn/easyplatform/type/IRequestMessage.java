/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.type;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a>
 *         <br/>
 * @since 5.0.0
 * <br/>
 */
public interface IRequestMessage<T> {

    /**
     * @return 后台id
     */
    Serializable getSessionId();

    /**
     * 设置会话id
     *
     * @param sessionId
     */
    void setSessionId(Serializable sessionId);

    /**
     * @return 当前工作任务的id
     */
    String getId();

    /**
     * @return
     */
    T getBody();
}
