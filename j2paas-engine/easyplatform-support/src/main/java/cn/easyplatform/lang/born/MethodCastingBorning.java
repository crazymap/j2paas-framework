/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.lang.born;

import cn.easyplatform.lang.Lang;

import java.lang.reflect.Method;

public class MethodCastingBorning<T> implements Borning<T> {

	private Method method;
	private Class<?>[] pts;

	public MethodCastingBorning(Method method) {
		this.method = method;
		this.method.setAccessible(true);
		this.pts = method.getParameterTypes();
	}

	@SuppressWarnings("unchecked")
	public T born(Object... args) {
		try {
			args = Lang.array2ObjectArray(args, pts);
			return (T) method.invoke(null, args);
		} catch (Exception e) {
			throw new BorningException(e, method.getDeclaringClass(), args);
		}
	}
}
