/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.dao.utils;

import cn.easyplatform.dao.dialect.Dialect;
import cn.easyplatform.dao.impl.db2.Db2Dialect;
import cn.easyplatform.dao.impl.derby.DerbyDialect;
import cn.easyplatform.dao.impl.dm.DmDialect;
import cn.easyplatform.dao.impl.mysql.MySqlDialect;
import cn.easyplatform.dao.impl.oracle.OracleDialect;
import cn.easyplatform.dao.impl.psql.PsqlDialect;
import cn.easyplatform.dao.impl.sqlserver.SqlServerDialect;
import cn.easyplatform.dao.impl.sybase.SybaseDialect;
import com.alibaba.druid.util.JdbcConstants;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public final class DaoUtils {

    public static final int DB2 = 1;

    public static final int ORACLE = 2;

    public static final int MYSQL = 3;

    public static final int SQLSERVER = 4;

    public static final int DERBY = 5;

    public static final int PSQL = 6;

    public static final int SYBASE = 7;

    public static final int DM = 8;//达梦

    /**
     * @param jdbcUrl
     * @return
     */
    public static int getDbType(String jdbcUrl) {
        String url = jdbcUrl.toLowerCase();
        url = url.substring(5);
        if (url.startsWith("oracle"))
            return ORACLE;
        if (url.startsWith("mysql"))
            return MYSQL;
        if (url.startsWith("sqlserver"))
            return SQLSERVER;
        if (url.startsWith("derby"))
            return DERBY;
        if (url.startsWith("db2"))
            return DB2;
        if (url.startsWith("postgresql"))
            return PSQL;
        if (url.startsWith("sybase"))
            return SYBASE;
        if (url.startsWith("dm"))
            return DM;
        return 0;
    }

    public static String getDatabaseType(String jdbcUrl) {
        String url = jdbcUrl.toLowerCase();
        url = url.substring(5);
        if (url.startsWith("oracle"))
            return JdbcConstants.ORACLE;
        if (url.startsWith("mysql"))
            return JdbcConstants.MYSQL;
        if (url.startsWith("sqlserver"))
            return JdbcConstants.SQL_SERVER;
        if (url.startsWith("derby"))
            return JdbcConstants.DERBY;
        if (url.startsWith("db2"))
            return JdbcConstants.DB2;
        if (url.startsWith("postgresql"))
            return JdbcConstants.POSTGRESQL;
        if (url.startsWith("sybase"))
            return JdbcConstants.SYBASE;
        if (url.startsWith("dm"))
            return JdbcConstants.DM;
        return JdbcConstants.H2;
    }

    public static Dialect getDialect(String jdbcUrl) {
        String url = jdbcUrl.toLowerCase();
        url = url.substring(5);
        if (url.startsWith("oracle"))
            return new OracleDialect();
        if (url.startsWith("mysql"))
            return new MySqlDialect();
        if (url.startsWith("sqlserver"))
            return new SqlServerDialect();
        if (url.startsWith("derby"))
            return new DerbyDialect();
        if (url.startsWith("db2"))
            return new Db2Dialect();
        if (url.startsWith("postgresql"))
            return new PsqlDialect();
        if (url.startsWith("sybase"))
            return new SybaseDialect();
        if (url.startsWith("dm"))
            return new DmDialect();
        return new MySqlDialect();
    }

    /**
     * @param conn
     * @throws SQLException
     */
    public static void close(Connection conn) throws SQLException {
        if (conn != null) {
            conn.close();
        }
    }

    /**
     * @param rs
     * @throws SQLException
     */
    public static void close(ResultSet rs) throws SQLException {
        if (rs != null) {
            rs.close();
            rs = null;
        }
    }

    /**
     * @param stmt
     * @throws SQLException
     */
    public static void close(Statement stmt) throws SQLException {
        if (stmt != null) {
            stmt.close();
            stmt = null;
        }
    }

    /**
     * @param conn
     */
    public static void closeQuietly(Connection conn) {
        try {
            close(conn);
        } catch (SQLException e) {
            // quiet
        }
    }

    /**
     * @param stmt
     * @param rs
     */
    public static void closeQuietly(Statement stmt, ResultSet rs) {
        closeQuietly(rs);
        closeQuietly(stmt);
    }

    /**
     * @param rs
     */
    public static void closeQuietly(ResultSet rs) {
        try {
            close(rs);
        } catch (SQLException e) {
            // quiet
        }
    }

    /**
     * @param stmt
     */
    public static void closeQuietly(Statement stmt) {
        try {
            close(stmt);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
