/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans.list;

import cn.easyplatform.entities.transform.TransformerFactory;
import cn.easyplatform.lang.Dumps;
import cn.easyplatform.type.FieldType;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ListTest {
	@Test
	public void testJava2Xml() {
		List<Header> headers1 = new ArrayList<Header>();

		Header header = new Header();
		header.setField("userName");
		header.setTitle("用户名称");
		header.setType(FieldType.VARCHAR);
		header.setWidth("100");
		headers1.add(header);

		header = new Header();
		header.setStyle("color:blue");
		header.setField("buyDate");
		header.setTitle("购买日期");
		header.setFormat("yyyy/MM/dd");
		header.setType(FieldType.DATETIME);
		header.setWidth("120");
		headers1.add(header);

		header = new Header();
		header.setField("prodName");
		header.setTitle("商品名称");
		header.setType(FieldType.VARCHAR);
		header.setWidth("220");
		headers1.add(header);

		header = new Header();
		header.setField("discount");
		header.setTitle("折扣");
		header.setType(FieldType.NUMERIC);
		header.setFormat("##,###.00");
		header.setWidth("20");
		headers1.add(header);

		header = new Header();
		header.setField("price");
		header.setTitle("原价");
		header.setFormat("##,###.00");
		header.setType(FieldType.NUMERIC);
		header.setWidth("120");
		headers1.add(header);

		header = new Header();
		header.setField("preprice");
		header.setTitle("优惠价");
		header.setType(FieldType.NUMERIC);
		header.setFormat("##,###.00");
		header.setWidth("120");
		//header.setTotal(true);
		header.setTotalName("分类统计金额：");
//		header.setGrandTotalName("总计金额:");
//		header.setTotalStyle("font-size:22px");
//		header.setGrandTotalStyle("font-size:30px");
		headers1.add(header);

		ListBean lb = new ListBean();
		lb.setPanel("<grid></grid>");
		lb.setHeaders(headers1);
		try {
			URL url = getClass().getResource(
					"/cn/easyplatform/entities/beans/list");
			File file = new File(url.getFile() + "/list1.xml");
			TransformerFactory.newInstance().transformToXml(lb,
					new FileOutputStream(file));
		} catch (Exception e) {
			e.printStackTrace();
		}

		/*List<AuxHead> auxHeads1 = new ArrayList<AuxHead>();
		List<AuxHeader> auxHeaders1 = new ArrayList<AuxHeader>();
		AuxHead auxhead = new AuxHead();
		auxhead.setAuxHeaders(auxHeaders1);
		auxHeads1.add(auxhead);
		
		AuxHeader ah = new AuxHeader();
		ah.setTitle("第一层标题头1");
		auxHeaders1.add(ah);

		lb.setAuxHeads(auxHeads1);
		lb.setHeaders(headers1);

		try {
			URL url = getClass().getResource(
					"/cn/easyplatform/entities/beans/list");
			File file = new File(url.getFile() + "/list2.xml");
			TransformerFactory.newInstance().transformToXml(lb,
					new FileOutputStream(file));
		} catch (Exception e) {
			e.printStackTrace();
		}

		auxHeaders1.clear();
		ah = new AuxHeader();
		ah.setTitle("第一层标题头1");
		auxHeaders1.add(ah);
		ah.setHeaders(headers1);

		ah = new AuxHeader();
		ah.setTitle("第一层标题头2");
		auxHeaders1.add(ah);

		List<Header> header2 = new ArrayList<Header>();
		ah.setHeaders(header2);

		header = new Header();
		header.setField("address");
		header.setTitle("地址");
		header.setWidth(120);
		header2.add(header);

		try {
			URL url = getClass().getResource(
					"/cn/easyplatform/entities/beans/list");
			File file = new File(url.getFile() + "/list3.xml");
			TransformerFactory.newInstance().transformToXml(lb,
					new FileOutputStream(file));
		} catch (Exception e) {
			e.printStackTrace();
		}

		auxHeaders1 = new ArrayList<AuxHeader>();
		ah = new AuxHeader();
		ah.setTitle("第一层标题头1");
		auxHeaders1.add(ah);

		List<AuxHeader> auxHeaders2 = new ArrayList<AuxHeader>();
		AuxHeader ah1 = new AuxHeader();
		ah1.setTitle("第二层标题头1");
		ah1.setHeaders(headers1);
		auxHeaders2.add(ah1);

		AuxHeader ah2 = new AuxHeader();
		ah2.setTitle("第二层标题头2");
		ah2.setHeaders(header2);
		auxHeaders2.add(ah2);

		ah.setAuxHeaders(auxHeaders2);
		
		lb.setAuxHeaders(auxHeaders1);
		
		try {
			URL url = getClass().getResource(
					"/cn/easyplatform/entities/beans/list");
			File file = new File(url.getFile() + "/list4.xml");
			TransformerFactory.newInstance().transformToXml(lb,
					new FileOutputStream(file));
		} catch (Exception e) {
			e.printStackTrace();
		}*/
	}

	@Test
	public void testXml2Java() {
		try {
			URL url = getClass().getResource(
					"/cn/easyplatform/entities/beans/list/list1.xml");
			ListBean lb = TransformerFactory.newInstance().transformFromXml(
					ListBean.class, url.openStream());
			System.out.println(Dumps.obj(lb));

			url = getClass().getResource(
					"/cn/easyplatform/entities/beans/list/list2.xml");
			lb = TransformerFactory.newInstance().transformFromXml(
					ListBean.class, url.openStream());
			System.out.println(Dumps.obj(lb));

			url = getClass().getResource(
					"/cn/easyplatform/entities/beans/list/list3.xml");
			lb = TransformerFactory.newInstance().transformFromXml(
					ListBean.class, url.openStream());
			System.out.println(Dumps.obj(lb));
			
			url = getClass().getResource(
					"/cn/easyplatform/entities/beans/list/list4.xml");
			lb = TransformerFactory.newInstance().transformFromXml(
					ListBean.class, url.openStream());
			System.out.println(Dumps.obj(lb));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
