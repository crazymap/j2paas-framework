/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans.report;

import cn.easyplatform.entities.BaseEntity;

import javax.xml.bind.annotation.*;

/**
 * @Author: <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @Description:
 * @Since: 2.0.0 <br/>
 * @Date: Created in 2019/10/23 13:32
 * @Modified By:
 */
@XmlType(propOrder = {"formulaType", "table", "customFunctions", "config", "template"})
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "report")
public class JxlsReport extends BaseEntity {

    /**
     * 对应表
     */
    @XmlElement
    private String table;

    /**
     * 使用公式类型
     * 0 ：不使用
     * 1 :使用FastFormulaProcessor
     * 2 ：使用StandardFormulaProcessor
     */
    @XmlElement
    private int formulaType;

    /**
     * 自定义函数
     */
    @XmlElement
    private String customFunctions;

    /**
     * 模板
     */
    @XmlElement
    private byte[] template;

    /**
     * xml格式配置
     */
    @XmlElement
    private String config;

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public int getFormulaType() {
        return formulaType;
    }

    public void setFormulaType(int formulaType) {
        this.formulaType = formulaType;
    }

    public String getCustomFunctions() {
        return customFunctions;
    }

    public void setCustomFunctions(String customFunctions) {
        this.customFunctions = customFunctions;
    }

    public byte[] getTemplate() {
        return template;
    }

    public void setTemplate(byte[] template) {
        this.template = template;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }
}
