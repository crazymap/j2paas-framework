/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.demo.batch;

import cn.easyplatform.EasyPlatformRuntimeException;
import cn.easyplatform.dao.utils.DaoUtils;
import cn.easyplatform.demo.AbstractDemoTest;
import cn.easyplatform.lang.Streams;
import cn.easyplatform.transaction.jdbc.JdbcTransactions;
import cn.easyplatform.type.EntityType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class BatchTest extends AbstractDemoTest {

	public void test1() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			log.debug("正在新建批处理");
			conn = JdbcTransactions.getConnection(ds1);

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.batch.1");
			pstmt.setString(2, "简单批处理");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.BATCH.getName());
			String bat1 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("bat_1.xml")));
			pstmt.setString(5, bat1);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();
			
			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.batch.2");
			pstmt.setString(2, "自已滚自已");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.BATCH.getName());
			String bat2 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("bat_2.xml")));
			pstmt.setString(5, bat2);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.batch.3");
			pstmt.setString(2, "多个串接");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.BATCH.getName());
			String bat3 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("bat_3.xml")));
			pstmt.setString(5, bat3);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();
			
			log.debug("新建批处理成功");
		} catch (SQLException ex) {
			throw new EasyPlatformRuntimeException("BatchTest", ex);
		} finally {
			DaoUtils.closeQuietly(pstmt);
		}
	}
}
