/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.auth;

import cn.easyplatform.dos.UserDo;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;

/**
 * @Author: <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @Description:
 * @Since: 2.0.0 <br/>
 * @Date: Created in 2019/5/9 17:26
 * @Modified By:
 */
public class DefaultAuthenticationInfo implements AuthenticationInfo {

    private UserDo user;

    private PrincipalCollection principals;

    public DefaultAuthenticationInfo(UserDo user) {
        this.user = user;
        this.principals = new SimplePrincipalCollection(user, "");
    }

    @Override
    public PrincipalCollection getPrincipals() {
        return this.principals;
    }

    @Override
    public Object getCredentials() {
        return this.user.getPassword();
    }
}
