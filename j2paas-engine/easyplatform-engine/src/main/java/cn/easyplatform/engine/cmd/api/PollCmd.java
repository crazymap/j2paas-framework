/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.api;

import cn.easyplatform.dao.BizDao;
import cn.easyplatform.dos.FieldDo;
import cn.easyplatform.i18n.I18N;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.h5.MsnVo;
import cn.easyplatform.messages.vos.h5.PushVo;
import cn.easyplatform.type.FieldType;
import cn.easyplatform.type.IResponseMessage;
import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static cn.easyplatform.messages.vos.h5.MessageVo.*;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PollCmd extends AbstractCommand<SimpleRequestMessage> {

    /**
     * @param req
     */
    public PollCmd(SimpleRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        MsnVo msg = (MsnVo) req.getBody();
        List<FieldDo> params = new ArrayList<FieldDo>();
        params.add(new FieldDo(FieldType.LONG, msg.getMsgid()));
        BizDao dao = cc.getBizDao();
        FieldDo data = dao.selectObject("select content from sys_notice_info where id=?", params);
        Object value = null;
        if (data != null) {
            value = data.getValue();
            if (!msg.getType().equals(TYPE_TASK)) {
                if (msg.getStatus() == STATE_UNREAD) {
                    params.clear();
                    params.add(new FieldDo(FieldType.INT, STATE_READED));
                    params.add(new FieldDo(FieldType.DATETIME, new Date()));
                    params.add(new FieldDo(FieldType.LONG, msg.getMsgid()));
                    params.add(new FieldDo(FieldType.VARCHAR, cc.getUser().getId()));
                    dao.update(
                            cc.getUser(),
                            "update sys_notice_detail_info set status=?,readtime=? where msgid=? and touser=?",
                            params, true);
                }
            } else
                value = JSON.parseObject((String) value, PushVo.class);
        } else
            return new SimpleResponseMessage("e024", I18N.getLabel("context.record.not.found", msg.getMsgid()));
        return new SimpleResponseMessage(value);
    }

}
