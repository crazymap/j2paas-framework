/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.service;

import cn.easyplatform.EngineService;
import cn.easyplatform.engine.cmd.admin.*;
import cn.easyplatform.engine.cmd.admin.cache.*;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.request.admin.ResourceSaveRequestMessage;
import cn.easyplatform.messages.request.admin.ServiceRequestMessage;
import cn.easyplatform.spi.service.AdminService;
import cn.easyplatform.type.IResponseMessage;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class AdminServiceImpl extends EngineService implements AdminService {

    @Override
    public IResponseMessage<?> getHome(SimpleRequestMessage req) {
        return commandExecutor.execute(new GetHomeCmd(req));
    }

    @Override
    public IResponseMessage<?> getService(ServiceRequestMessage req) {
        return commandExecutor.execute(new GetServiceCmd(req));
    }

    @Override
    public IResponseMessage<?> getServices(SimpleRequestMessage req) {
        return commandExecutor.execute(new GetServicesCmd(req));
    }

    @Override
    public IResponseMessage<?> setServiceState(ServiceRequestMessage req) {
        return commandExecutor.execute(new SetServiceStateCmd(req));
    }

    @Override
    public IResponseMessage<?> getModel(SimpleRequestMessage req) {
        return commandExecutor.execute(new GetModelCmd(req));
    }

    @Override
    public IResponseMessage<?> getData(SimpleRequestMessage req) {
        return commandExecutor.execute(new GetDataCmd(req));
    }

    @Override
    public IResponseMessage<?> saveProject(SimpleRequestMessage req) {
        return commandExecutor.execute(new SaveProjectCmd(req));
    }

    @Override
    public IResponseMessage<?> saveResource(ResourceSaveRequestMessage req) {
        return commandExecutor.execute(new SaveResourceCmd(req));
    }

    @Override
    public IResponseMessage<?> getConfig(SimpleRequestMessage req) {
        return commandExecutor.execute(new GetConfigCmd(req));
    }

    @Override
    public IResponseMessage<?> initCache(SimpleRequestMessage req) {
        return commandExecutor.execute(new InitCmd(req));
    }

    @Override
    public IResponseMessage<?> enableCache(SimpleRequestMessage req) {
        return commandExecutor.execute(new EnableCmd(req));
    }

    @Override
    public IResponseMessage<?> getCacheList(SimpleRequestMessage req) {
        return commandExecutor.execute(new GetListCmd(req));
    }

    @Override
    public IResponseMessage<?> resetCache(SimpleRequestMessage req) {
        return commandExecutor.execute(new ResetCmd(req));
    }

    @Override
    public IResponseMessage<?> reloadCache(SimpleRequestMessage req) {
        return commandExecutor.execute(new ReloadCmd(req));
    }

    @Override
    public IResponseMessage<?> deleteCache(SimpleRequestMessage req) {
        return commandExecutor.execute(new DeleteCmd(req));
    }

    @Override
    public IResponseMessage<?> getLog(SimpleRequestMessage req) {
        return commandExecutor.execute(new GetLogCmd(req));
    }

    @Override
    public IResponseMessage<?> console(SimpleRequestMessage req) {
        return commandExecutor.execute(new ConsoleCmd(req));
    }

    @Override
    public IResponseMessage<?> table(SimpleRequestMessage req) {
        return commandExecutor.execute(new TableCmd(req));
    }
}
