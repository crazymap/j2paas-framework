/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.component;

import cn.easyplatform.engine.runtime.ComponentProcessFactory;
import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.DoReportRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.ReportVo;
import cn.easyplatform.type.EntityType;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.MessageUtils;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DoReportCmd extends AbstractCommand<DoReportRequestMessage> {

    /**
     * @param req
     */
    public DoReportCmd(DoReportRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        ReportVo rv = req.getBody();
        BaseEntity bean = cc.getEntity(rv.getEntityId());
        if (bean == null)
            return MessageUtils.entityNotFound(EntityType.REPORT.getName(),
                    rv.getEntityId());
        return new SimpleResponseMessage(ComponentProcessFactory
                .createComponentProcess(bean).doComponent(cc, rv, bean));
    }

}
