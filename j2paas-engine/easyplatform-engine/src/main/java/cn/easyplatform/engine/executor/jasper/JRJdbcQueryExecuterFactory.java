/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.executor.jasper;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.query.AbstractQueryExecuterFactory;
import net.sf.jasperreports.engine.query.JRQueryExecuter;

import java.util.Arrays;
import java.util.Map;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class JRJdbcQueryExecuterFactory extends AbstractQueryExecuterFactory {
	
	/**
	 * Property specifying the ResultSet fetch size.
	 */
	public static final String PROPERTY_JDBC_FETCH_SIZE = JRPropertiesUtil.PROPERTY_PREFIX
			+ "jdbc.fetch.size";

	/**
	 * Property specifying the ResultSet type.
	 */
	public static final String PROPERTY_JDBC_RESULT_SET_TYPE = JRPropertiesUtil.PROPERTY_PREFIX
			+ "jdbc.result.set.type";

	/**
	 * Property specifying the ResultSet concurrency.
	 */
	public static final String PROPERTY_JDBC_CONCURRENCY = JRPropertiesUtil.PROPERTY_PREFIX
			+ "jdbc.concurrency";

	/**
	 * Property specifying the ResultSet holdability.
	 */
	public static final String PROPERTY_JDBC_HOLDABILITY = JRPropertiesUtil.PROPERTY_PREFIX
			+ "jdbc.holdability";

	/**
	 * Property specifying the statement max field size.
	 */
	public static final String PROPERTY_JDBC_MAX_FIELD_SIZE = JRPropertiesUtil.PROPERTY_PREFIX
			+ "jdbc.max.field.size";

	/**
	 * Flag property specifying if data will be stored in a cached rowset.
	 */
	public static final String PROPERTY_CACHED_ROWSET = JRPropertiesUtil.PROPERTY_PREFIX
			+ "jdbc.cached.rowset";

	/**
	 * Property specifying the default time zone to be used for sending and
	 * retrieving date/time values to and from the database.
	 * 
	 * In most cases no explicit time zone conversion would be required, and
	 * this property should not be set.
	 * 
	 * <p>
	 * The property can be set globally, at dataset level, at parameter and
	 * field levels, and as a report/dataset parameter. Note that sending a
	 * value as parameter will override all properties, and the time zone will
	 * be used for all date/time parameters and fields in the report.
	 * </p>
	 * 
	 * @see JRResultSetDataSource#setTimeZone(java.util.TimeZone, boolean)
	 */
	public static final String PROPERTY_TIME_ZONE = JRPropertiesUtil.PROPERTY_PREFIX
			+ "jdbc.time.zone";

	/**
	 * SQL query language.
	 */
	public static final String QUERY_LANGUAGE_SQL = "sql";

	private static final String[] queryParameterClassNames;

	static {
		queryParameterClassNames = new String[] {
				java.lang.Object.class.getName(),
				java.lang.Boolean.class.getName(),
				java.lang.Byte.class.getName(),
				java.lang.Double.class.getName(),
				java.lang.Float.class.getName(),
				java.lang.Integer.class.getName(),
				java.lang.Long.class.getName(),
				java.lang.Short.class.getName(),
				java.math.BigDecimal.class.getName(),
				java.lang.String.class.getName(),
				java.util.Date.class.getName(), java.sql.Date.class.getName(),
				java.sql.Timestamp.class.getName(),
				java.sql.Time.class.getName() };

		Arrays.sort(queryParameterClassNames);
	}

	public JRQueryExecuter createQueryExecuter(
			JasperReportsContext jasperReportsContext, JRDataset dataset,
			Map<String, ? extends JRValueParameter> parameters)
			throws JRException {
		return new JRJdbcQueryExecuter(jasperReportsContext, dataset,
				parameters);
	}

	public Object[] getBuiltinParameters() {
		return null;
	}

	public boolean supportsQueryParameterType(String className) {
		return Arrays.binarySearch(queryParameterClassNames, className) >= 0;
	}
}
