/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.identity;

import cn.easyplatform.dos.LogDo;
import cn.easyplatform.dos.UserDo;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.log.LogManager;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.services.IProjectService;
import cn.easyplatform.type.IResponseMessage;
import org.apache.shiro.subject.Subject;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class LogoutCmd extends AbstractCommand<SimpleRequestMessage> {

    /**
     * @param req
     */
    public LogoutCmd(SimpleRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        if (req.getBody() == null) {
            cc.logout();
            LogManager.stopUser(cc.getUser());
        } else {//单点登陆
            String[] data = (String[]) req.getBody();
            IProjectService ps = cc.getEngineConfiguration().getProjectService(data[0]);
            if (ps != null) {
                UserDo user = ps.getSessionManager().getUser(data[1]);
                if (user != null) {
                    try {
                        req = new SimpleRequestMessage();
                        req.setSessionId(user.getSessionId());
                        LogoutCmd cmd = new LogoutCmd(req);
                        cc = new CommandContext(cc.getEngineConfiguration(), cmd);
                        cmd.execute(cc);
                    } catch (Exception e) {//不抛错误
                    }
                }
            }
        }
        return new SimpleResponseMessage();
    }

}
