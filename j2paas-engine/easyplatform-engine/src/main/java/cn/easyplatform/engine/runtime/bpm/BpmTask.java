/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.runtime.bpm;

import cn.easyplatform.contexts.WorkflowContext;
import cn.easyplatform.engine.runtime.page.PageTask;
import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.entities.beans.bpm.BpmBean;
import cn.easyplatform.entities.beans.task.TaskBean;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.type.EntityType;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.MessageUtils;
import cn.easyplatform.util.RuntimeUtils;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class BpmTask extends PageTask {

	@Override
	public IResponseMessage<?> doTask(CommandContext cc, BaseEntity bean) {
		BpmBean bb = (BpmBean) bean;
		String taskId = cc.getBpmEngine().getInstanceTask(bb.getId());
		if (taskId == null)
			return MessageUtils.entityNotFound(EntityType.TASK.getName(),
					taskId);
		TaskBean task = cc.getEntity(taskId);
		if (task == null)
			return MessageUtils.entityNotFound(EntityType.TASK.getName(),taskId);
		BaseEntity base = cc.getEntity(task.getRefId());
		if (base == null)
			return MessageUtils.entityNotFound(EntityType.PAGE.getName(),
					task.getRefId());
		WorkflowContext ctx = cc.getWorkflowContext();
		RuntimeUtils.initBpm(ctx, bb);
		return super.doTask(cc, base);
	}

}
