/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.admin;

import cn.easyplatform.dao.EntityDao;
import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.entities.beans.project.DeviceMapBean;
import cn.easyplatform.entities.beans.project.PortletBean;
import cn.easyplatform.entities.beans.project.ProjectBean;
import cn.easyplatform.entities.transform.TransformerFactory;
import cn.easyplatform.i18n.I18N;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.admin.ModelVo;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.EntityUtils;
import cn.easyplatform.util.MessageUtils;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetModelCmd extends AbstractCommand<SimpleRequestMessage> {

    /**
     * @param req
     */
    public GetModelCmd(SimpleRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        ModelVo mv = (ModelVo) req.getBody();
        EntityInfo entity = cc.getEntityDao().getModel(mv.getId());
        if (entity == null)
            return MessageUtils.entityNotFound("Model", mv.getId());
        BaseEntity baseEntity = TransformerFactory.newInstance().transformFromXml(
                entity);
        if (Strings.isBlank(mv.getType()))
            return new SimpleResponseMessage(EntityUtils.entity2Vo(baseEntity));
        else {
            ProjectBean project = (ProjectBean) baseEntity;
            if ("template".equals(mv.getType())) {
                EntityDao dao = cc.getEntityDao();
                for (DeviceMapBean dm : project.getDevices()) {
                    if (dm.getLoginId() > 0) {
                        String content = (String) dao.selectObject("SELECT content FROM ep_model_ext_info WHERE id=? AND projectId=?", dm.getLoginId(), project.getId());
                        if (content == null)
                            return new SimpleResponseMessage(I18N.getLabel("app.page.not.found", project.getId(), dm.getMainId()));
                        dm.setLoginPage(content);
                    }
                    if (dm.getMainId() > 0) {
                        String content = (String) dao.selectObject("SELECT content FROM ep_model_ext_info WHERE id=? AND projectId=?", dm.getMainId(), project.getId());
                        if (content == null)
                            return new SimpleResponseMessage(I18N.getLabel("app.page.not.found", project.getId(), dm.getMainId()));
                        dm.setMainPage(content);
                    }
                }
                if (project.getPortlets() != null && !project.getPortlets().isEmpty()) {
                    for (PortletBean pb : project.getPortlets()) {
                        for (DeviceMapBean dm : pb.getDevices()) {
                            if (dm.getLoginId() > 0) {
                                String content = (String) dao.selectObject("SELECT content FROM ep_model_ext_info WHERE id=? AND projectId=?", dm.getLoginId(), project.getId());
                                if (content == null)
                                    return new SimpleResponseMessage(I18N.getLabel("app.page.not.found", project.getId(), dm.getMainId()));
                                dm.setLoginPage(content);
                            }
                            if (dm.getMainId() > 0) {
                                String content = (String) dao.selectObject("SELECT content FROM ep_model_ext_info WHERE id=? AND projectId=?", dm.getMainId(), project.getId());
                                if (content == null)
                                    return new SimpleResponseMessage(I18N.getLabel("app.page.not.found", project.getId(), dm.getMainId()));
                                dm.setMainPage(content);
                            }
                        }
                    }
                }
            }
            return new SimpleResponseMessage(EntityUtils.cast(project, mv.getType()));
        }
    }

}
