
package cn.easyplatform.report.sub;

import java.util.HashMap;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

import org.junit.Test;

import cn.easyplatform.report.AbstractReportTest;
import cn.easyplatform.report.JREasyPlatformScriptlet;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SubReportTest extends AbstractReportTest {

	@Test
	public void test() throws Exception {
		JasperReport jr = JasperCompileManager
				.compileReport(SubReportTest.class
						.getResourceAsStream("subreport_1.jrxml"));
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		for (JRParameter p : jr.getParameters()) {
			if (p.getValueClass() == JasperReport.class
					&& !p.getName().equals("JASPER_REPORT")) {
				parameters.put(p.getName(), JasperCompileManager
						.compileReport(SubReportTest.class
								.getResourceAsStream(p.getName() + ".jrxml")));
			}
			System.out.println(p.getName() + ":" + p.isSystemDefined());
		}
		parameters.put("REPORT_CONNECTION", getConn());
		parameters.put("REPORT_SCRIPTLET", new JREasyPlatformScriptlet());

		JasperPrint jasperPrint = JasperFillManager.fillReport(jr, parameters);
		JasperViewer viewer = new JasperViewer(jasperPrint);
		viewer.setVisible(true);
		synchronized (this) {
			this.wait();
		}
	}
}
