/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.contexts;

import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.type.FieldVo;

import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ExecuteContext {

    private String taskId;

    private List<FieldVo> variables;
    private String code;

    private CommandContext cc;

    public ExecuteContext(CommandContext cc, String taskId) {
        this.taskId = taskId;
        this.cc = cc;
    }

    public ExecuteContext(CommandContext cc, String taskId, List<FieldVo> variables) {
        this.taskId = taskId;
        this.variables = variables;
        this.cc = cc;
    }

    public ExecuteContext(CommandContext cc, String taskId, String code) {
        this.taskId = taskId;
        this.code = code;
        this.cc = cc;
    }

    public String getTaskId() {
        return taskId;
    }

    public String getCode() {
        return code;
    }

    public CommandContext getCommandContext() {
        return cc;
    }

    public List<FieldVo> getVariables() {
        return variables;
    }
}
